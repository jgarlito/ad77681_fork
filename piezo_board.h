/******************************************************************************
 *Copyright (c)2019 Analog Devices, Inc.  
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:  
 *  - Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.  
 *  - Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of  conditions and the following disclaimer in 
 *    the documentation and/or other materials provided with the 
 *    distribution.    
 *  - Modified versions of the software must be conspicuously marked 
 *    as such.  
 *  - This software is licensed solely and exclusively for use with 
 *    processors/products manufactured by or for Analog Devices, Inc.  
 *  - This software may not be combined or merged with other code 
 *    in any manner that would cause the software to   become subject 
 *    to terms and conditions which differ from those listed here.  
 *  - Neither the name of Analog Devices, Inc. nor the names of its
 *    contributors may be used to endorseor promote products derived 
 *    from this software without specific prior written permission.  
 *  - The use of this software may or may not infringe the patent 
 *    rights of one or more patent holders.  
 *  
 *  This license does not release you from the requirement that you obtain
 *  separate licenses from these patent holders to use this software.
 *  
 * THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES, INC. AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, NON-INFRINGEMENT, TITLE, MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL ANALOG 
 * DEVICES, INC. OR CONTRIBUTORS BE LIABLE FORANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, DAMAGES ARISING OUT OF CLAIMS 
 * OFINTELLECTUAL PROPERTY RIGHTS INFRINGEMENT; PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVERCAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef _PIEZO_BOARD_H_
#define _PIEZO_BOARD_H_


#include <stdint.h>
#include <stdbool.h>
#include "ad77681.h"
#include "pcal9555a.h"
#include "ltc2606.h"
#include "ad77681.h"

/**
 ******************************************************************************************************************************************************************
 ******************************************************************************************************************************************************************
 * 
 *			MACROS AND CONSTANT DEFINITIONS
 */

#define LED1	1
#define LED2	2

#ifndef AD7768_FULL_SCALE
#define AD7768_FULL_SCALE (1 << 24)				  
#endif // !AD7768_FULL_SCALE


/**
 ******************************************************************************************************************************************************************
 ******************************************************************************************************************************************************************
 * 
 *			TYPES DECLARATIONS
 */

const float buffer_gain			= 1 + (1.27 / 5.9); 			// Gain factor of the on-board DAC buffer, to have full 5V range
																// Non-inverting op-amp resistor ratio => 1 + (1.27 k ohm / 5.9 k ohm)
const float first_stage_gain		= 15 / 49.9;				// Gain factor of the first stage, set by resistor ratio 
																// Inverting op-amp resistor ratio => 15 k ohm / 49.9 k ohm
const float second_stage_gain		= 2 / 0.75;					// Gain factor of the second stage
																// Inverting op-amp resistor ratio => 2 k ohm / 0.75 k ohm

enum ltc6373_gain						// Gain setting of PGA LTC6373
{ 
	LTC6373_G_16		= 0b000,  		//	16
	LTC6373_G_8			= 0b100,  		//	8
	LTC6373_G_4			= 0b010,  		//	4
	LTC6373_G_2			= 0b110,  		//	2
	LTC6373_G_1			= 0b001,  		//	1
	LTC6373_G_05		= 0b101,  		//	0.5
	LTC6373_G_025		= 0b011,   		//	0.25
	LTC6373_G_0			= 0b111			//	Shutdown
};

enum io_interface						// Different pins controlling interface
{
	AD7768_1_GPIOS,
	ARDUINO_GPIOS,
	IO_EXTENDER,
	RESISTORS
};

enum ltc2606_buffer						// Buffer for the DAC LTC2606
{
	LTC2606_BUFFER_ENABLE  = 1,
	LTC2606_BUFFER_DISABLE = 0
};

enum piezo_protecion_switch				// Buffer for the DAC LTC2606
{
	PIEZO_SWITCH_ENABLE  = 1,
	PIEZO_SWITCH_DISABLE = 0
};

enum ada4945							// Power mode and enable of FDA ADA4945-1
{
	ADA4945_LOW_POWER = 0,	 			// Low power mode
	ADA4945_FULL_POWER = 1,
	ADA4945_ENABLE = 1,		 			// ADA4945-1 Enable
	ADA4945_DISABLE = 0
};

enum adc_mode
{
	AD7768_PIN_MODE = 0,
	AD7768_SPI_MODE = 1
};

struct arduino_gpios_init
{
	gpio_desc			led1;		
	gpio_desc			led2;
};

struct arduino_gpios
{
	gpio_desc			*led1;
	gpio_desc			*led2;
};

/**
 ******************************************************************************************************************************************************************
 ******************************************************************************************************************************************************************
 * 
 *			FUNCTION DECLARATIONS
 */

int32_t LTC6373_gain(struct ad77681_dev *dev_adc, struct pcal9555a_dev *dev_io, enum ltc6373_gain gain, enum io_interface interface);
int32_t ADA4945_mode(struct ad77681_dev *dev_adc, struct pcal9555a_dev *dev_io, enum ada4945 mode, enum io_interface interface);
int32_t ADA4945_enable(struct pcal9555a_dev *dev_io, enum ada4945 ctrl, enum io_interface interface);
int32_t	LTC2606_buffer(struct pcal9555a_dev *dev_io, enum ltc2606_buffer ctrl, enum io_interface interface);
int32_t reset_adc(struct pcal9555a_dev *dev_io, enum io_interface interface);
int32_t set_adc_pin_spi_mode(struct pcal9555a_dev *dev_io, enum adc_mode mode);
int32_t protection_swtich(struct pcal9555a_dev *dev_io, enum piezo_protecion_switch ctrl, enum io_interface interface);
int32_t board_init(struct ad77681_dev *dev_adc, struct pcal9555a_dev *dev_io);
int32_t arduino_gpio_init(struct arduino_gpios **gpios, struct arduino_gpios_init init_params);
int32_t led(struct arduino_gpios *gpios, uint8_t led_number, bool value);
int32_t piezo_offset_compenzation(struct ad77681_dev *dev_adc, struct ltc2606_dev *dev_dac, struct adc_data *measured_data, uint16_t *compenzated_dac_code);
void get_mean_code(struct adc_data *measured_data, double *mean_code);
void sampling(struct ad77681_dev *dev_adc, struct adc_data *measured_data);
#endif // !_PIEZO_BOARD_H_
