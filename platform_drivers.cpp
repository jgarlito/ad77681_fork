/***************************************************************************//**
 *   @file   platform_drivers.cpp
 *   @brief  Implementation of Generic Platform Drivers.
 *   @author DBogdan (dragos.bogdan@analog.com)
********************************************************************************
 * Copyright 2017,2019(c) Analog Devices, Inc.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  - Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *  - Neither the name of Analog Devices, Inc. nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *  - The use of this software may or may not infringe the patent rights
 *    of one or more patent holders.  This license does not release you
 *    from the requirement that you obtain separate licenses from these
 *    patent holders to use this software.
 *  - Use of the software either in source or binary form, must be run
 *    on or directly connected to an Analog Devices Inc. component.
 *
 * THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT,
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL ANALOG DEVICES BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, INTELLECTUAL PROPERTY RIGHTS, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include <stdint.h>
#include <mbed.h>
#include "platform_drivers.h"

/**
	Provide implementations for the following extern functions.
	For Example - in main.cpp
		PinName slave_selects[8] = { SPI_CS, SPI_CSE };
		mbed::SPI spi(SPI_MOSI, SPI_MISO, SPI_SCK);
		mbed::I2C i2c(I2C_SDA, I2C_SCL);
 **/

extern SPI spi;
extern I2C i2c;
extern PinName slave_selects[MAX_SLAVE_SELECTS];
/******************************************************************************/
/************************ Functions Definitions *******************************/
/******************************************************************************/

/**
 * @brief Initialize the I2C communication peripheral.
 * @param desc - The I2C descriptor.
 * @param param - The structure that contains the I2C parameters.
 * @return SUCCESS in case of success, FAILURE otherwise.
 */
int32_t mbed_i2c_init(struct i2c_desc **desc,
	const struct i2c_init_param *param)
{
	i2c_desc * new_desc = (i2c_desc*) malloc(sizeof(i2c_desc));
	new_desc->id = param->id;
	new_desc->slave_address = param->slave_address;
	new_desc->type = param->type;

	*desc = new_desc;

	return SUCCESS;
}


/**
 * @brief Free the resources allocated by i2c_init().
 * @param desc - The I2C descriptor.
 * @return SUCCESS in case of success, FAILURE otherwise.
 */
int32_t i2c_remove(struct i2c_desc *desc)
{
	if (desc) {
		// Unused variable - fix compiler warning
	}

	return SUCCESS;
}

/**
 * @brief Write data to a slave device.
 * @param desc - The I2C descriptor.
 * @param data - Buffer that stores the transmission data.
 * @param bytes_number - Number of bytes to write.
 * @param stop_bit - Stop condition control.
 *                   Example: 0 - A stop condition will not be generated;
 *                            1 - A stop condition will be generated.
 * @return SUCCESS in case of success, FAILURE otherwise.
 */

int32_t mbed_i2c_write(struct i2c_desc *desc,
	uint8_t *data,
	uint8_t bytes_number,
	uint8_t stop_bit)
{
/**
	The MBED I2C API is reversed for parameter 4
	Instead of stop_bit - it has
	@param repeated	  - Repeated start, true - don't send stop at end default value is false.
	Inverting here to keep the no-OS/platform_drivers API
 */
	if(!(i2c.write(desc->slave_address , (char *)data, bytes_number, !stop_bit)))
		return SUCCESS;
	else
		return FAILURE;
}

/**
 * @brief Read data from a slave device.
 * @param desc - The I2C descriptor.
 * @param data - Buffer that will store the received data.
 * @param bytes_number - Number of bytes to read.
 * @param stop_bit - Stop condition control.
 *                   Example: 0 - A stop condition will not be generated;
 *                            1 - A stop condition will be generated.
 * @return SUCCESS in case of success, FAILURE otherwise.
 */
int32_t mbed_i2c_read(struct i2c_desc *desc,
	uint8_t *data,
	uint8_t bytes_number,
	uint8_t stop_bit)
{
/**
	The MBED I2C API is reversed for parameter 4
	Instead of stop_bit - it has
	@param repeated	  - Repeated start, true - don't send stop at end default value is false.
	Inverting here to keep the no-OS/platform_drivers API
 */
	if(!(i2c.read(desc->slave_address, (char *)data, bytes_number, !stop_bit)))
		return SUCCESS;
	else
		return FAILURE;
}

/**
 * @brief Initialize the SPI communication peripheral.
 * @param desc - The SPI descriptor.
 * @param init_param - The structure that contains the SPI parameters.
 * @return SUCCESS in case of success, FAILURE otherwise.
 */
int32_t mbed_spi_init(struct spi_desc **desc,
	const struct spi_init_param *param)
{

   // Create the spi description object for the device
	spi_desc * new_desc = (spi_desc*) malloc(sizeof(*new_desc));
	new_desc->chip_select = param->chip_select;
	new_desc->mode = param->mode;
	new_desc->max_speed_hz = param->max_speed_hz;
	*desc = new_desc;
     //Create the DigitalOut object, based on user-defined chip-select
    DigitalOut SS(slave_selects[new_desc->chip_select]);
	spi.format(SEND_BYTE, param->mode); 		//Stick to byte-multiples
	/**
		NOTE: Actual frequency of SPI clk will be somewhat device
		dependent, relating to clock-settings, prescalars etc. If absolute
		SPI frequency is required, consult your device documentation.
	 **/
	spi.frequency(param->max_speed_hz);
	spi.set_default_write_value(0x00); 	//code to write when reading back
	SS = GPIO_HIGH; 							//set SS high

	return SUCCESS;
}

/**
 * @brief Free the resources allocated by spi_init().
 * @param desc - The SPI descriptor.
 * @return SUCCESS in case of success, FAILURE otherwise.
 */
int32_t spi_remove(struct spi_desc *desc)
{
	if (desc) {
		// Unused variable - fix compiler warning
	}

	return SUCCESS;
}


/**
 * @brief Write and read data to/from SPI.
 *
 * This function will be updated to improve performance
 *
 * @param desc - The SPI descriptor.
 * @param data - The buffer with the transmitted/received data.
 * @param bytes_number - Number of bytes to write/read.
 * @return SUCCESS in case of success, FAILURE otherwise.
 */
int32_t spi_write_and_read(struct spi_desc *desc,
	uint8_t *data,
	uint8_t bytes_number)
{
	if (desc) {
		// Unused variable - fix compiler warning
	}
	//Create the DigitalOut object, based on user-defined chip-select
    DigitalOut SS(slave_selects[desc->chip_select]);

	SS = GPIO_LOW; 							//!select SS
	for(size_t byte = 0 ; byte < bytes_number ; byte++)
	{
		data[byte] = spi.write(data[byte]);
	}
	SS = GPIO_HIGH; 							//!deselect SS

	return SUCCESS;
}

/**
 * @brief Obtain the GPIO decriptor.
 * @param desc - The GPIO descriptor.
 * @param gpio_number - The number of the GPIO.
 * @return SUCCESS in case of success, FAILURE otherwise.
 */
int32_t gpio_get(struct gpio_desc **desc,
	gpio_desc init_values)
{
	gpio_desc * new_gpio = new gpio_desc;
	new_gpio->id = init_values.id;
	new_gpio->number = init_values.number;
	new_gpio->pin = init_values.pin;
	new_gpio->type = init_values.type;

	*desc = new_gpio;

	return SUCCESS;
}

/**
 * @brief Free the resources allocated by gpio_get().
 * @param desc - The SPI descriptor.
 * @return SUCCESS in case of success, FAILURE otherwise.
 */
int32_t gpio_remove(struct gpio_desc *desc)
{
	if (desc) {
		// Unused variable - fix compiler warning
	}

	return SUCCESS;
}

/**
 * @brief Enable the input direction of the specified GPIO.
 * @param desc - The GPIO descriptor.
 * @return SUCCESS in case of success, FAILURE otherwise.
 */
int32_t gpio_direction_input(struct gpio_desc *desc)
{
	//unnessary for MBED
	if(desc) { }
		// Unused variable - fix compiler warning

	return 0;
}

/**
 * @brief Enable the output direction of the specified GPIO.
 * @param desc - The GPIO descriptor.
 * @param value - The value.
 *                Example: GPIO_HIGH
 *                         GPIO_LOW
 * @return SUCCESS in case of success, FAILURE otherwise.
 */
int32_t gpio_direction_output(struct gpio_desc *desc,
	uint8_t value)
{
	//unnessary for MBED ?
	if(desc) { }
		// Unused variable - fix compiler warning

	return SUCCESS;
}

/**
 * @brief Get the direction of the specified GPIO.
 * @param desc - The GPIO descriptor.
 * @param direction - The direction.
 *                    Example: GPIO_OUT
 *                             GPIO_IN
 * @return SUCCESS in case of success, FAILURE otherwise.
 */
int32_t gpio_get_direction(struct gpio_desc *desc,
	uint8_t *direction)
{
	if (desc) {
		// Unused variable - fix compiler warning
	}

	if (direction) {
		// Unused variable - fix compiler warning
	}

	return SUCCESS;
}

/**
 * @brief Set the value of the specified GPIO.
 * @param desc - The GPIO descriptor.
 * @param value - The value.
 *                Example: GPIO_HIGH
 *                         GPIO_LOW
 * @return SUCCESS in case of success, FAILURE otherwise.
 */
int32_t gpio_set_value(struct gpio_desc *desc,
	uint8_t value)
{
	if (desc)
		DigitalOut(desc->pin, value);
	else
		return FAILURE;

	return SUCCESS;
}

/**
 * @brief Get the value of the specified GPIO.
 * @param desc - The GPIO descriptor.
 * @param value - The value.
 *                Example: GPIO_HIGH
 *                         GPIO_LOW
 * @return SUCCESS in case of success, FAILURE otherwise.
 */
int32_t gpio_get_value(struct gpio_desc *desc, uint8_t *value)
{
	uint8_t returnVal = FAILURE;
	if (desc) {
		{
			DigitalIn gpio(desc->pin);
			*value = (uint8_t)gpio.read();
			*value = gpio;
			returnVal = gpio.is_connected() ? SUCCESS : FAILURE;
		}

		return returnVal;
	}
	return FAILURE;
}

/**
 * @brief Generate miliseconds delay.
 * @param msecs - Delay in miliseconds.
 * @return None.
 */
void mdelay(uint32_t msecs)
{
	if (msecs)
	{
		//unused variable - fix compiler warning
	}
}
