/*!
LTC2607: 16-Bit, Dual Rail-to-Rail DACs with I2C Interface.
LTC2609: Quad 16-/14-/12-Bit Rail-to-Rail DACs with I�C Interface.
LTC2606: 16-Bit Rail-to-Rail DACs with I�C Interface.

@verbatim

The LTC2607/LTC2617/LTC2627 are dual 16-, 14- and 12-bit, 2.7V to 5.5V
rail-to-rail voltage output DACs in a 12-lead DFN package. They have built-in
high performance output buffers and are guaranteed monotonic.

These parts establish new board-density benchmarks for 16- and 14-bit DACs and
advance performance standards for output drive and load regulation in single-
supply, voltage-output DACs.

The parts use a 2-wire, I2C compatible serial interface. The
LTC2607/LTC2617/LTC2627 operate in both the standard mode (clock rate of 100kHz)
and the fast mode (clock rate of 400kHz). An asynchronous DAC update pin (LDAC)
is also included.

The LTC2607/LTC2617/LTC2627 incorporate a power-on reset circuit. During power-
up, the voltage outputs rise less than 10mV above zero scale; and after power-
up, they stay at zero scale until a valid write and update take place. The
power-on reset circuit resets the LTC2607-1/LTC2617-1/ LTC2627-1 to mid-scale.
The voltage outputs stay at midscale until a valid write and update takes place.

I2C DATA FORMAT (MSB First):

       Byte #1                             Byte #2                          Byte #3                              Byte #4

START  SA6 SA5 SA4 SA3 SA2 SA1 SA0 W SACK  C3 C2 C1 C0  A3  A2 A1 A0  SACK  D15 D14 D13 D12 D11 D10 D9 D8  SACK  D7 D6  D5  D4  D3  D2  D1  D0 SACK STOP

SACK : Slave Acknowlege
SAx  : I2C Address
W    : I2C Write (0)
R    : I2C Read  (1)
SACK : I2C Slave Generated Acknowledge (Active Low)
Cx   : DAC Command Code
Ax   : DAC Address
Dx   : DAC Data Bits
X    : Don't care


Example Code:

Set DAC A to Full Scale.

    dac_code = 0x0FFFF; // Set dac code to full scale

    // Write dac code to the LTC2607 and update dac
    ack = LTC2607_write(LTC2607_I2C_GLOBAL_ADDRESS, LTC2607_WRITE_UPDATE_COMMAND, LTC2607_DAC_A, dac_code);

@endverbatim

http://www.linear.com/product/LTC2607
http://www.linear.com/product/LTC2609
http://www.linear.com/product/LTC2606

http://www.linear.com/product/LTC2607#demoboards
http://www.linear.com/product/LTC2609#demoboards
http://www.linear.com/product/LTC2606#demoboards

REVISION HISTORY
 $Revision: 4780 $
 $Date: 2016-03-14 13:58:55 -0700 (Mon, 14 Mar 2016) $

Copyright (c) 2013, Linear Technology Corp.(LTC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of Linear Technology Corp.

The Linear Technology Linduino is not affiliated with the official Arduino team.
However, the Linduino is only possible because of the Arduino team's commitment
to the open-source community.  Please, visit http://www.arduino.cc and
http://store.arduino.cc , and consider a purchase that will help fund their
ongoing work.
*/

/*! @file
    @ingroup LTC2607
    Header File for LTC2607: 16-Bit, Dual Rail-to-Rail DACs with I2C Interface
*/


#ifndef _LTC2606_H_
#define _LTC2606_H_

#include <stdint.h>
#include "platform_drivers.h"

/**
 ******************************************************************************************************************************************************************
 ******************************************************************************************************************************************************************
 * 
 *			MACROS AND CONSTANT DEFINITIONS
 */


// LTC2606 I2C Address                 //  AD2       AD1       AD0
#define LTC2606_I2C_ADDRESS 0x10       //  LOW       LOW       LOW
// #define LTC2606_I2C_ADDRESS 0x11    //  LOW       LOW       Float
// #define LTC2606_I2C_ADDRESS 0x12    //  LOW       LOW       HIGH
// #define LTC2606_I2C_ADDRESS 0x13    //  LOW       Float     LOW
// #define LTC2606_I2C_ADDRESS 0x20    //  LOW       Float     Float
// #define LTC2606_I2C_ADDRESS 0x21    //  LOW       Float     High	
// #define LTC2606_I2C_ADDRESS 0x22    //  LOW       HIGH      LOW
// #define LTC2606_I2C_ADDRESS 0x23    //  LOW       HIGH      Float
// #define LTC2606_I2C_ADDRESS 0x30    //  LOW       High      HIGH
// #define LTC2606_I2C_ADDRESS 0x31    //  Float     LOW       LOW
// #define LTC2606_I2C_ADDRESS 0x32    //  Float     LOW       Float
// #define LTC2606_I2C_ADDRESS 0x33    //  Float     LOW       HIGH
// #define LTC2606_I2C_ADDRESS 0x40    //  Float     Float     LOW
// #define LTC2606_I2C_ADDRESS 0x41    //  Float     Float     Float
// #define LTC2606_I2C_ADDRESS 0x42    //  Float     Float     HIGH
// #define LTC2606_I2C_ADDRESS 0x43    //  Float     High      LOW
// #define LTC2606_I2C_ADDRESS 0x50    //  Float     High      Float
// #define LTC2606_I2C_ADDRESS 0x51    //  Float     High      HIGH
// #define LTC2606_I2C_ADDRESS 0x52    //  High      LOW       LOW
// #define LTC2606_I2C_ADDRESS 0x53    //  High      LOW       Float
// #define LTC2606_I2C_ADDRESS 0x60    //  High      LOW       High
// #define LTC2606_I2C_ADDRESS 0x61    //  High      Float     LOW
// #define LTC2606_I2C_ADDRESS 0x62    //  High      Float     Float
// #define LTC2606_I2C_ADDRESS 0x63    //  High      Float     High
// #define LTC2606_I2C_ADDRESS 0x70    //  High      High      LOW
// #define LTC2606_I2C_ADDRESS 0x71    //  High      High      Float
// #define LTC2606_I2C_ADDRESS 0x72    //  High      High      High


#define LTC2606_I2C_GLOBAL_ADDRESS		0x73			//  Global Address

#define LTC2606_WRITE_COMMAND			0x00			// Command to write to internal register of LTC2606, but not update output voltage yet.
#define LTC2606_UPDATE_COMMAND			0x10			// Command to update (and power up) LTC2606. Output voltage will be set to the value stored in the internal register by previous write command.
#define LTC2606_WRITE_UPDATE_COMMAND	0x30			// Command to write and update (and power up) the LTC2606. The output voltage will immediate change to the value being written to the internal register.
#define LTC2606_POWER_DOWN_COMMAND		0x40			// Command to power down the LTC2606.

#define LTC2606_WRITE_ADDRESS(x)		(x << 1)		// I2C addres shift, to make R/W bit LOW by default

// Return error values
#define LTC2606_CODE_OVERFLOW			-2
#define LTC2606_CODE_UNDERFLOW			-3

// Fullscale and Halfscale bits
#define LTC2606_FULL_SCALE_BITS			65535			// DAC Fullscale = 2^16 - 1
#define LTC2606_HALF_SCALE_BITS			32767			// DAC Halfscale = 2^15 - 1
#define LTC2606_QUARTER_SCALE_BITS		16383			// DAC quarter scale = 2^15 - 2^14 -1
#define LTC2606_3QUARTER_SCALE_BITS		49151			// DAC 3-quarter scale = 2^15 + 2^14 -1


/**
 ******************************************************************************************************************************************************************
 ******************************************************************************************************************************************************************
 * 
 *			TYPES DECLARATIONS
 */


struct ltc2606_dev {
	/* I2C */
	i2c_desc	*i2c_desc;
	uint8_t		resolution_setting;
	
}
;

struct ltc2606_init_param {
	/* I2C */
	i2c_init_param	i2c_init;
	/* Device Settings */
	uint8_t		resolution_setting;
};

/**
 ******************************************************************************************************************************************************************
 ******************************************************************************************************************************************************************
 * 
 *			FUNCTION DECLARATIONS
 */


int32_t ltc2606_init(struct ltc2606_dev **device, struct ltc2606_init_param init_param);
int16_t ltc2606_voltage_to_code(float dac_voltage, uint16_t *code);
int32_t ltc2606_write_code(struct ltc2606_dev *dev, uint8_t dac_command, uint16_t dac_code);
int32_t ltc2606_power_down(struct ltc2606_dev *dev);
int32_t ltc2606_power_up(struct ltc2606_dev *dev);

#endif // !_LTC2606_H_

