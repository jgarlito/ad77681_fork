/******************************************************************************
 *Copyright (c)2019 Analog Devices, Inc.  
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:  
 *  - Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.  
 *  - Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of  conditions and the following disclaimer in 
 *    the documentation and/or other materials provided with the 
 *    distribution.    
 *  - Modified versions of the software must be conspicuously marked 
 *    as such.  
 *  - This software is licensed solely and exclusively for use with 
 *    processors/products manufactured by or for Analog Devices, Inc.  
 *  - This software may not be combined or merged with other code 
 *    in any manner that would cause the software to   become subject 
 *    to terms and conditions which differ from those listed here.  
 *  - Neither the name of Analog Devices, Inc. nor the names of its
 *    contributors may be used to endorseor promote products derived 
 *    from this software without specific prior written permission.  
 *  - The use of this software may or may not infringe the patent 
 *    rights of one or more patent holders.  
 *  
 *  This license does not release you from the requirement that you obtain
 *  separate licenses from these patent holders to use this software.
 *  
 * THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES, INC. AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, NON-INFRINGEMENT, TITLE, MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL ANALOG 
 * DEVICES, INC. OR CONTRIBUTORS BE LIABLE FORANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, DAMAGES ARISING OUT OF CLAIMS 
 * OFINTELLECTUAL PROPERTY RIGHTS INFRINGEMENT; PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVERCAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>

#include "piezo_board.h"
#include "platform_drivers.h"
#include "adc_drivers.h"
#include "ad77681.h"
#include "pcal9555a.h"
#include "ltc2606.h"
#include "debug.h"
#include "interrupts.h"


/**
 * Controll gain of the input PGA LTC6373
 * @param dev_io			The io_extender device structure
 * @param dev_adc			The ADC AD7768-1 device structure
 * @param gain				Gain of the PGA
 *							Accepted values:	LTC6373_G_16
 *												LTC6373_G_4
 *												LTC6373_G_2
 *												LTC6373_G_1
 *												LTC6373_G_05
 *												LTC6373_G_025
 *												LTC6373_G_0
 *												
 * @param interface			Controlling interface
 *							Accepted values:	ARDUINO_GPIOS
 *												IO_EXTENDER
 *												AD7768_1_GPIOS
 *												
 * @return 0 in case of success, negative error code otherwise.
 */
int32_t LTC6373_gain(struct ad77681_dev *dev_adc, struct pcal9555a_dev *dev_io, enum ltc6373_gain gain, enum io_interface interface)
{
	int32_t ret;
	int8_t i;
	uint8_t value = 0;
	
	switch (interface)
	{
	case AD7768_1_GPIOS: {
		
			ret |= ad77681_global_gpio(dev_adc, AD77681_GLOBAL_GPIO_ENABLE); 				// Global GPIO enable for AD7768-1
			ret |= ad77681_gpio_inout(dev_adc, GPIO_OUT, AD77681_GPIO0); 				// Set ADCs GPIO0 - GPIO2 as outputs
			ret |= ad77681_gpio_inout(dev_adc, GPIO_OUT, AD77681_GPIO1);
			ret |= ad77681_gpio_inout(dev_adc, GPIO_OUT, AD77681_GPIO2);
		
			for (i = 0; i < 3; i++)
			{
				(gain & 0b100) ? (value = 1) : (value = 0);	
				ret |= ad77681_gpio_write(dev_adc, value, i);
				gain <<= 1;
			}
		
			break;
		}
		
	case IO_EXTENDER: {
		
			for (i = 0; i < 3; i++)
			{
				(gain & 0b100) ? (value = 1) : (value = 0);	
				ret |= pcal9555a_write_gpio_register(dev_io, OUTPUT_PORT_1, i, value);
				gain <<= 1;
			}
		
			break;
		}
	default:
		break;
	}
	
	return ret;
}


/**
 * Controll the FDA ADA4945-1 mode
 * @param dev_io			The io_extender device structure
 * @param dev_adc			The ADC AD7768-1 device structure
 * @param mode				Mode of the FDA
 *							Accepted values:	ADA4945_LOW_POWER
 *												ADA4945_FULL_POWER
 *												
 * @param interface			Controlling interface
 *							Accepted values:	ARDUINO_GPIOS
 *												IO_EXTENDER
 *												AD7768_1_GPIOS
 *												
 * @return 0 in case of success, negative error code otherwise.
 */
int32_t ADA4945_mode(struct ad77681_dev *dev_adc, struct pcal9555a_dev *dev_io, enum ada4945 mode, enum io_interface interface)
{
	int32_t ret;
	
	switch (interface)
	{
	case AD7768_1_GPIOS:
		ret |= ad77681_global_gpio(dev_adc, AD77681_GLOBAL_GPIO_ENABLE);  				// Global GPIO enable for AD7768-1
		ret |= ad77681_gpio_inout(dev_adc, GPIO_OUT, AD77681_GPIO3); 				// Set ADCs GPIO3 as output
		
		ret |= ad77681_gpio_write(dev_adc, mode, AD77681_GPIO3);
		break;
		
	case IO_EXTENDER:
		ret |= pcal9555a_write_gpio_register(dev_io, OUTPUT_PORT_1, PCAL9555A_PIN3, mode);
		break;
		
	default:
		break;
	}
	
	return ret;
}


/**
 * Controll the FDA ADA4945-1 enable / disable
 * @param dev_io			The io_extender device structure
 * @param ctrl				Controll command for the protection switch
 *							Accepted values:	ADA4945_ENABLE
 *												ADA4945_DISABLE
 *												
 * @param interface			Controlling interface
 *							Accepted values:	ARDUINO_GPIOS
 *												IO_EXTENDER
 *												
 * @return 0 in case of success, negative error code otherwise.
 */
int32_t ADA4945_enable(struct pcal9555a_dev *dev_io, enum ada4945 ctrl, enum io_interface interface)
{
	int32_t ret;
	
	switch (interface)
	{
	case IO_EXTENDER:
		ret |= pcal9555a_write_gpio_register(dev_io, OUTPUT_PORT_1, PCAL9555A_PIN4, ctrl);
		break;
		
	case ARDUINO_GPIOS:
		break;
		
	default:
		break;		
	}
	
	return ret;
}


/**
 * Controll the DAC LTC2606 buffer
 * @param dev_io			The io_extender device structure
 * @param ctrl				Controll command for the protection switch
 *							Accepted values:	LTC2606_BUFFER_ENABLE
 *												LTC2606_BUFFER_DISABLE
 *												
 * @param interface			Controlling interface
 *							Accepted values:	ARDUINO_GPIOS
 *												IO_EXTENDER
 *												
 * @return 0 in case of success, negative error code otherwise.
 */
int32_t	LTC2606_buffer(struct pcal9555a_dev *dev_io, enum ltc2606_buffer ctrl, enum io_interface interface)
{
	int32_t ret;
	
	switch (interface)
	{
	case IO_EXTENDER:
		ret |= pcal9555a_write_gpio_register(dev_io, OUTPUT_PORT_1, PCAL9555A_PIN5, ctrl);
		break;
		
	case ARDUINO_GPIOS:
		break;
		
	default:
		break;		
	}
	
	return ret;
}


/**
 * Reset ADC by toggling RESET pin
 * @param dev_io			The io_extender device structure
 * @param interface			Controlling interface
 *							Accepted values:	ARDUINO_GPIOS
 *												IO_EXTENDER
 *												
 * @return 0 in case of success, negative error code otherwise.
 */
int32_t reset_adc(struct pcal9555a_dev *dev_io, enum io_interface interface)
{
	int32_t ret;
	
	switch (interface)
	{
	case IO_EXTENDER:
		ret |= pcal9555a_write_gpio_register(dev_io, OUTPUT_PORT_0, PCAL9555A_PIN5, 0);
		_delay_ms(20);
		ret |= pcal9555a_write_gpio_register(dev_io, OUTPUT_PORT_0, PCAL9555A_PIN5, 1);
		break;
		
	case ARDUINO_GPIOS:
		break;
		
	default:
		break;		
	}
	
	return ret;
}


/**
 * Set communitcating mode of the ADC, SPI mode or PIN mode, SPI mode is selected by externall pull-up by default
 * @param dev_io			The io_extender device structure
 * @param mode				Desired ADC mode
 *							Accepted values:	AD7768_PIN_MODE
 *												AD7768_SPI_MODE
 *												
 * @return 0 in case of success, negative error code otherwise.
 */
int32_t set_adc_pin_spi_mode(struct pcal9555a_dev *dev_io, enum adc_mode mode)
{	
	
	return pcal9555a_write_gpio_register(dev_io, OUTPUT_PORT_0, PCAL9555A_PIN4, mode);
	
}


/**
 * Controll the input protection switch on piezo board
 * @param dev_io			The io_extender device structure
 * @param ctrl				Controll command for the protection switch
 *							Accepted values:	PIEZO_SWITCH_ENABLE
 *												PIEZO_SWITCH_DISABLE
 *												
 * @param interface			Controlling interface
 *							Accepted values:	ARDUINO_GPIOS
 *												IO_EXTENDER
 *												
 * @return 0 in case of success, negative error code otherwise.
 */
int32_t protection_swtich(struct pcal9555a_dev *dev_io, enum piezo_protecion_switch ctrl, enum io_interface interface)
{
	int32_t ret;
	
	switch (interface)
	{
	case IO_EXTENDER:
		ret |= pcal9555a_write_gpio_register(dev_io, OUTPUT_PORT_1, PCAL9555A_PIN6, ctrl);
		break;
		
	case ARDUINO_GPIOS:
		break;
		
	default:
		break;		
	}
	
	return ret;
}


/**
 * Initialize board
 * @param dev_io			The io_extender device structure
 * @param dev_adc			The ADC AD7768-1 device structure
 * @return 0 in case of success, negative error code otherwise.
 */
int32_t board_init(struct ad77681_dev *dev_adc, struct pcal9555a_dev *dev_io)
{
	/*	Setting pin directions on the io-extender - pcal9555a
	 *	
	 *	Port 0	P0_0 -	ADC's GPIO0								IN			Pull-none Set to Input, as high impedance pin		
	 *			P0_1 -	ADC's GPIO1								IN			Pull-none Set to Input, as high impedance pin	
	 *			P0_2 -	ADC's GPIO2								IN			Pull-none Set to Input, as high impedance pin
	 *			P0_3 -	ADC's GPIO3								IN			Pull-none Set to Input, as high impedance pin
	 *			P0_4 -	PIN_N/SPI								OUT			Pull-none		
	 *			P0_5 -	RESET_ADC								IN			Pull-none (external Pull-up)						Not used primary, set as a high-impedance input
	 *			P0_6 -	Not used								IN			Pull-none
	 *			P0_7 -	Not used								IN			Pull-none		
	 *			
	 *	Port 1	P1_0 -	A0 at GPIO LTC6373						OUT			Pull-up
	 *			P1_1 -	A1 at GPIO LTC6373						OUT			Pull-up
	 *			P1_2 -	A2 at GPIO LTC6373						OUT			Pull-up
	 *			P1_3 -	FDA mode								OUT			Pull-up
	 *			P1_4 -	FDA enable								OUT			Pull-none (external Pull-up)
	 *			P1_5 -	DAC buffer enable						OUT			Pull-up
	 *			P1_6 -	SW_IN - Switch at piezo's input			OUT			Pull-none (external Pull-up)
	 *			P1_7 -	SW_FF - Fault Flag at piezo's input		IN				Pull-none
	 *
	 *
	 *
	 **/
	
	int32_t ret;
	
	// GPIO direction setting
	ret |= pcal9555a_write_gpio_register(dev_io, CONFIGURATION_0, PCAL9555A_ALL_PINS, 0b11101111);
	ret |= pcal9555a_write_gpio_register(dev_io, CONFIGURATION_1, PCAL9555A_ALL_PINS, 0b10000000);
	
	// Pulls connecting/disconnecting
	ret |= pcal9555a_write_gpio_register(dev_io, PUPD_ENABLE_0, PCAL9555A_ALL_PINS, 0x00);
	ret |= pcal9555a_write_gpio_register(dev_io, PUPD_ENABLE_1, PCAL9555A_ALL_PINS, 0b00001111);
	
	// Pull-up / Pull-down selection, Pull-up = 1, Pull-down = 0
	ret |= pcal9555a_write_gpio_register(dev_io, PUPD_SELECTION_0, PCAL9555A_ALL_PINS, 0xFF);  				// 0xFF by default
	ret |= pcal9555a_write_gpio_register(dev_io, PUPD_SELECTION_1, PCAL9555A_ALL_PINS, 0b00101111); 		// Fault Flag switched back
	
	// Enable interrupt on SW_FF - Port 1, pin 7
	//ret |= pcal9555a_write_gpio_register(dev_io, INTERRUPT_MASK_1, PCAL9555A_ALL_PINS, 0b01111111);
	
	// Setting PGA gain to 1 by default
	LTC6373_gain(dev_adc, dev_io, LTC6373_G_1, IO_EXTENDER);
	
	// Full power of FDA ADA4945-1
	ADA4945_mode(dev_adc, dev_io, ADA4945_FULL_POWER, IO_EXTENDER);
	
	// Enable FDA ADA4945-1
	ADA4945_enable(dev_io, ADA4945_ENABLE, IO_EXTENDER);
	
	// Enable LTC2606 buffer
	LTC2606_buffer(dev_io, LTC2606_BUFFER_ENABLE, IO_EXTENDER);
	
	// Enable protection switch at piezo board
	protection_swtich(dev_io, PIEZO_SWITCH_ENABLE, IO_EXTENDER);
	
	return ret;
}

/**
 * Initialize the arduino GPIOS
 * @param gpios			The gpios structure.
 * @param init_params	The structure that contains the initial	parameters.
 * @return 0 in case of success, negative error code otherwise.
 */
int32_t arduino_gpio_init(struct arduino_gpios **gpios, struct arduino_gpios_init init_params)
{
	int32_t ret;
	struct arduino_gpios *ard_gpios;
	ard_gpios = (struct arduino_gpios *)malloc(sizeof(*ard_gpios));
	if (!ard_gpios)
		return -1;
	
	ret |= gpio_get(&ard_gpios->led1, init_params.led1);
	ret |= gpio_get(&ard_gpios->led2, init_params.led2);
	
	*gpios = ard_gpios;
	
	return ret;
}

/**
 * Controll LED on board
 * @param gpios			The gpios structure.
 * @param led_number	LED number
 *						Accepted values:	LED1
 *											LED2
 * @param value			LED on/off
 * @return 0 in case of success, negative error code otherwise.
 */
int32_t led(struct arduino_gpios *gpios, uint8_t led_number, bool value)
{
	int32_t ret;
	
	switch (led_number)
	{
	case LED1:
		ret |= gpio_set_value(gpios->led1, value);
		break;
	case LED2:
		ret |= gpio_set_value(gpios->led2, value);
		break;
		
	default:
		ret = -1;
		break;
	}
	
	return ret;
}

/**
 * Offset compensation of the piezo sensor
 * @param dev_adc				the ADC AD7768-1 structure
 * @param dev_dac				the DAC LTC2606 strucutre
 * @param measured_data			structure with the measured data and its settings
 * @param compenzated_dac_code	the final result of the compenzation process
 * @return 0 in case of success, negative error code otherwise.
 * 
 * The offset compenzation process uses a successive approximation model
 * There is lot of averaging going on, because of quite noisy piezo accelerometer
 * The whole process takes several minutes and the final value can be stored and used for further measurements
 *
 */
int32_t piezo_offset_compenzation(struct ad77681_dev *dev_adc, struct ltc2606_dev *dev_dac, struct adc_data *measured_data, uint16_t *compenzated_dac_code)
{
	bool dac_voltage;
	int8_t dac_search = -3;
	uint8_t i, j, min_mean_code_position, fine_mean_code_position;
	uint16_t dac_code = 0, fine_dac_code = 0, SINC3_odr;
	double mean_code = 0.0, min_mean_code = AD7768_FULL_SCALE;
	float shifting_voltage, mean_voltage;
	char print_buf[150];
	char up[3] = { 'U', 'P' }, down[5] = { 'D', 'O', 'W', 'N' };
	
	// Ratio between ADC and DAC LSBS, one DAC LSB is approx 540 ADC LSBs
	// The ratio set by DAC buffer gain, 1st and 2nd stage gains and full-scale bits of DAC and ADC itself.
	const float DAC_ADC_LSBs_ratio = (AD7768_FULL_SCALE * buffer_gain * (first_stage_gain + 1) * second_stage_gain) / (2 * LTC2606_FULL_SCALE_BITS);
	
	typedef struct cal_data
	{
		uint16_t	dac_codes[16];
		float		mean_codes[16];
	}cal_data;
	
	cal_data coarse_cal_data, fine_cal_data;								
	
	// Low power mode and MCLK/16
	ad77681_set_power_mode(dev_adc, AD77681_ECO);
	ad77681_set_mclk_div(dev_adc, AD77681_MCLK_DIV_16);	
	
	ad77681_SINC3_ODR(dev_adc, &SINC3_odr, 4); 																			// 4SPS = 7999 SINC3, 10SPS = 3199 SINC3, 50SPS = 639 SINC3
	ad77681_set_filter_type(dev_adc, AD77681_SINC5_FIR_DECx32, AD77681_SINC3, SINC3_odr); 								// Set the oversamplig ratio to high value, to extract DC 

	for(j = 0 ; j < 10 ; j++)																							// Protective loop
	{
																														// In case of the result of the coarse compenzation is too high, 
																														// caused by enviromental noise picked up by the sensor, repeat the coarse cal.
		dac_code = LTC2606_HALF_SCALE_BITS; 																			// Initialize shifting voltage by aplying DAC half scale
		ltc2606_write_code(dev_dac, LTC2606_WRITE_UPDATE_COMMAND, dac_code); 											// Apply DAC codes to set the output value of the DAC
		coarse_cal_data.dac_codes[0] = dac_code;
		_delay_ms(500); 																								// Wait for output of the DAC to settle
	
		sprintf(print_buf, "Coarse approximation, DAC code:\t\t%x\n\n", dac_code);
		print_debug(print_buf);	
	
	
		/*
		 *	=====	Coarse Successive approximation		=======
		 *	Uses the successive approximation model
		 **/
		
		for (i = 1; i < 17; i++)
		{
			if (i < 5)																									// Faster step at the beginning
				measured_data->samples = 50;
			else
				measured_data->samples = 200;
		
			sampling(dev_adc, measured_data);  																			// Take X number of samples
			get_mean_code(measured_data, &mean_code);																	// Get the mean code of taken samples stroed in the measured_data strucutre			
			mean_voltage = ((2.0 * ((float)(dev_adc->vref))) / 1000.0 * (float)(mean_code)) / AD7768_FULL_SCALE;    	// Calculate DC offset voltage

			if (i < 16)																									
			{
				if (mean_code > 0)
				{
					dac_code += (0x10000 >> (i + 1)); 																	// If mean code is greater than zero, add 2^x to existing DAC code
					dac_voltage = true;  																				// to increase the DAC output
				}
			
				else
				{
					dac_code -= (0x10000 >> (i + 1)); 																	// If it is lower, substract 2^x, to decrease output of the DAC
					dac_voltage = false;
				}
				
				coarse_cal_data.dac_codes[i] = dac_code; 																// Saving DAC codes and mean codes into structure, for final
				coarse_cal_data.mean_codes[i - 1] = mean_code;
				
				ltc2606_write_code(dev_dac, LTC2606_WRITE_UPDATE_COMMAND, dac_code); 									// Update output of the DAC
				_delay_ms(500); 																						// Wait for DAC output to settle

				shifting_voltage = dac_code * buffer_gain * ((float)(dev_adc->vref) / LTC2606_FULL_SCALE_BITS);
				sprintf(print_buf,	"\n%d\nMean code:\t\t%.2f\n"
									"Mean voltage:\t\t%.4f mV\n\n"
									"New dac code:\t\t%x\n"
									"Shifting voltage:\t%.4f\n"
									"Voltage %s\n",	
										i,
										mean_code,
										mean_voltage * 1000,
										dac_code,
										shifting_voltage,
										((dac_voltage) ? (up) : (down)));
				print_debug(print_buf);	
			}
		
			else																										// Last iteration, where only mean codes need to be find
				{
					coarse_cal_data.mean_codes[i - 1] = mean_code;
					sprintf(print_buf,	"\n%d\nMean code:\t\t%.2f\n"
										"Mean voltage:\t\t%.4f mV\n\n",
											i,
											mean_code,
											mean_voltage * 1000);
					print_debug(print_buf);	
				}
		}
	
		// Finding the lowest mean code measured
		for(i = 0 ; i < 16 ; i++)
		{
			if (fabs(coarse_cal_data.mean_codes[i]) < min_mean_code)
			{
				min_mean_code = fabs(coarse_cal_data.mean_codes[i]);
				min_mean_code_position = i;
			}
		}
		
		if ((j == 9) && (fabs(coarse_cal_data.mean_codes[min_mean_code_position]) > DAC_ADC_LSBs_ratio * 2))			// Exit the compenzation function after 10 unsuccessfull trials
			{
				sprintf(print_buf, "\n == Coarse compenzation result not sufficien ! == \n No more Cal trials. Replace the sensor and run the Cal. again..\n\n");
				print_debug(print_buf);
				*compenzated_dac_code = 0;
				return -1;
			}
		
		else if (fabs(coarse_cal_data.mean_codes[min_mean_code_position]) > DAC_ADC_LSBs_ratio * 2)						// If the mean code is too high, more than 2 DAC/ADC steps, repeat the measurement
			{
				sprintf(print_buf, "\n == Coarse compenzation result not sufficien ! == \n Repeating the compenzation..\n\n");
				print_debug(print_buf);	
				min_mean_code = AD7768_FULL_SCALE;
			}

		else																											// If the mean code is good enough, continue to fine compenzation
			break;
	}
	
	mean_voltage = ((2.0 * ((float)(dev_adc->vref))) / 1000.0 * (float)(coarse_cal_data.mean_codes[min_mean_code_position])) / AD7768_FULL_SCALE;
	
	sprintf(print_buf,	"\n\n == Final result of the COARSE COMPENZATION ==\n"
						"\nMin mean code:\t\t%.2f\n"
						"Min mean voltage:\t%.4f mV\n"
						"DAC code:\t\t%x set as the final result\n\n\n", 
							coarse_cal_data.mean_codes[min_mean_code_position], 
							mean_voltage * 1000, 
							coarse_cal_data.dac_codes[min_mean_code_position]);
	print_debug(print_buf);	

	
	
	fine_dac_code = coarse_cal_data.dac_codes[min_mean_code_position] + dac_search; 								// Take the closest DAC code
	fine_cal_data.dac_codes[0] = fine_dac_code;
	
	ltc2606_write_code(dev_dac, LTC2606_WRITE_UPDATE_COMMAND, fine_dac_code); 										// Update output of the DAC with the fine DAC code
	_delay_ms(500);   																								// Wait for DAC output to settle
	shifting_voltage = buffer_gain * fine_dac_code * ((float)(dev_adc->vref) / LTC2606_FULL_SCALE_BITS);
	
	sprintf(print_buf,	"Fine approximation\n"
						"DAC code:\t\t%x\n"
						"Shifting voltage:\t%.4f\n", 
							fine_dac_code, 
							shifting_voltage);
	print_debug(print_buf);	
	
	measured_data->samples = 200;
	
	/*
	*	=====	Fine Successive approximation		=======
	*	Takes the best result from the Coarse approximation - The DAC code, when the mean code was closest to zero
	*	Afterwards, it looks for the lowest absolute mean code by setting the DAC code up to 3 LSBs lower and higher than the best result from the coarse compenzation
	**/
	
	for(i = 1 ; i < 8 ; i++)
	{
		sampling(dev_adc, measured_data);    																		// Take X number of samples
		get_mean_code(measured_data, &mean_code);																	// Get the mean code of taken samples stroed in the measured_data strucutre	
		mean_voltage = ((2.0 * ((float)(dev_adc->vref))) / 1000.0 * (float)(mean_code)) / AD7768_FULL_SCALE;     		// Calculate DC offset voltage
		
		if (i < 7)
		{
			dac_search++; 																							// Increase the DAC searching range and add it to the existing closest ADC code
			fine_dac_code = coarse_cal_data.dac_codes[min_mean_code_position] + dac_search;
			
			fine_cal_data.dac_codes[i] = fine_dac_code;  															// Saving DAC codes and mean codes into structure, for final
			fine_cal_data.mean_codes[i - 1] = mean_code;
				
			ltc2606_write_code(dev_dac, LTC2606_WRITE_UPDATE_COMMAND, fine_dac_code);  								// Update output of the DAC
			_delay_ms(500);  																						// Wait for DAC output to settle

			shifting_voltage = fine_dac_code * buffer_gain * ((float)(dev_adc->vref) /LTC2606_FULL_SCALE_BITS);
			
			sprintf(print_buf,	"\n%d\nMean code:\t\t%.2f\n"
								"Mean voltage:\t\t%.4f mV\n\n"
								"New dac code:\t\t%x\n"
								"Shifting voltage:\t%.4f\n",
									i,
									mean_code,
									mean_voltage * 1000,
									fine_dac_code,
									shifting_voltage);
			print_debug(print_buf);	
		}
		
		else																										// Last iteration, where only mean codes need to be find
			{
				fine_cal_data.mean_codes[i - 1] = mean_code;
				sprintf(print_buf,	"\n%d\nMean code:\t\t%.2f\n"
									"Mean voltage:\t\t%.4f mV\n\n",
										i,
										mean_code,
										mean_voltage * 1000);
				print_debug(print_buf);	
			}
	}
	
	min_mean_code = AD7768_FULL_SCALE;
	// Finding the lowest mean code measured
	for(i = 0 ; i < 7 ; i++)
	{
		if (fabs(fine_cal_data.mean_codes[i]) < min_mean_code)
		{
			min_mean_code = fabs(fine_cal_data.mean_codes[i]);
			fine_mean_code_position = i;
		}
	}
	
	ltc2606_write_code(dev_dac, LTC2606_WRITE_UPDATE_COMMAND, fine_cal_data.dac_codes[fine_mean_code_position]);      		// Update output of the DAC	
	mean_voltage = ((2.0 * ((float)(dev_adc->vref))) / 1000.0 * (float)(fine_cal_data.mean_codes[fine_mean_code_position])) / AD7768_FULL_SCALE;
	
	sprintf(print_buf,	"\n\n == Final result of the FINE COMPENZATION ==\n"
						"\nMin mean code:\t\t%.2f\n"
						"Min mean voltage:\t%.4f mV\n"
						"DAC code:\t\t%x set as the final result\n",
							fine_cal_data.mean_codes[fine_mean_code_position],
							mean_voltage * 1000,
							fine_cal_data.dac_codes[fine_mean_code_position]);
	print_debug(print_buf);
	*compenzated_dac_code = fine_cal_data.dac_codes[fine_mean_code_position];
	
	ad77681_set_filter_type(dev_adc, AD77681_SINC5_FIR_DECx32, AD77681_FIR, 0);
	ad77681_update_sample_rate(dev_adc);
	return 0;	
}


/**
 * Take desired amount of samples
 * @param dev_adc		The ADC structure
 * @param measured_data	The structure carying measured data
 */
void sampling(struct ad77681_dev *dev_adc, struct adc_data *measured_data)
{
	measured_data->finish = false; 															// Set finish flag to false
	measured_data->count = 0; 																// Zero the coun
	
	ad77681_set_continuos_read(dev_adc, AD77681_CONTINUOUS_READ_ENABLE);
	set_spi_format(dev_adc->spi_desc, _16bit_SPI);
	spi_slave_select(GPIO_LOW);
	drdy_interrupt_enable(dev_adc->gpio_drdy, ENABLE);
	
	while (!measured_data->finish) ;
}


/**
 * Get mean from sampled data
 * @param mean_code		Final mean code
 * @param measured_data	The structure carying measured data
 */
void get_mean_code(struct adc_data *measured_data, double *mean_code)
{
	int32_t shifted_data;
	int64_t sum = 0;
	
	for (measured_data->count = 0; measured_data->count < measured_data->samples; measured_data->count++)				// Conversion to shifted, signed codes
		{
			if (measured_data->raw_data[measured_data->count] & 0x800000)
				shifted_data = (int32_t)((0xFF << 24) | measured_data->raw_data[measured_data->count]);	
			else
				shifted_data = (int32_t)((0x00 << 24) | measured_data->raw_data[measured_data->count]);	
		
			sum += shifted_data;    																					// Sum of all codes for mean value
		}
	
	*mean_code = (double)(sum) / (double)(measured_data->samples);   													// Get DC offset by calculating mean value of samples	
	
}
