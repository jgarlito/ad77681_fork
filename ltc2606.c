/*!
LTC2607: 16-Bit, Dual Rail-to-Rail DACs with I2C Interface.
LTC2609: Quad 16-/14-/12-Bit Rail-to-Rail DACs with I�C Interface.
LTC2606: 16-Bit Rail-to-Rail DACs with I�C Interface.

@verbatim

The LTC2607/LTC2617/LTC2627 are dual 16-, 14- and 12-bit, 2.7V to 5.5V
rail-to-rail voltage output DACs in a 12-lead DFN package. They have built-in
high performance output buffers and are guaranteed monotonic.

These parts establish new board-density benchmarks for 16- and 14-bit DACs and
advance performance standards for output drive and load regulation in single-
supply, voltage-output DACs.

The parts use a 2-wire, I2C compatible serial interface. The
LTC2607/LTC2617/LTC2627 operate in both the standard mode (clock rate of 100kHz)
and the fast mode (clock rate of 400kHz). An asynchronous DAC update pin (LDAC)
is also included.

The LTC2607/LTC2617/LTC2627 incorporate a power-on reset circuit. During power-
up, the voltage outputs rise less than 10mV above zero scale; and after power-
up, they stay at zero scale until a valid write and update take place. The
power-on reset circuit resets the LTC2607-1/LTC2617-1/ LTC2627-1 to mid-scale.
The voltage outputs stay at midscale until a valid write and update takes place.

@endverbatim

http://www.linear.com/product/LTC2607
http://www.linear.com/product/LTC2609
http://www.linear.com/product/LTC2606

http://www.linear.com/product/LTC2607#demoboards
http://www.linear.com/product/LTC2609#demoboards
http://www.linear.com/product/LTC2606#demoboards

REVISION HISTORY
$Revision: 4780 $
$Date: 2016-03-14 13:58:55 -0700 (Mon, 14 Mar 2016) $

Copyright (c) 2013, Linear Technology Corp.(LTC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of Linear Technology Corp.

The Linear Technology Linduino is not affiliated with the official Arduino team.
However, the Linduino is only possible because of the Arduino team's commitment
to the open-source community.  Please, visit http://www.arduino.cc and
http://store.arduino.cc , and consider a purchase that will help fund their
ongoing work.
*/
//! @defgroup LTC2607 LTC2607: 16-Bit, Dual Rail-to-Rail DACs with I2C Interface

/*! @file
    @ingroup LTC2607
    Library for LTC2607: 16-Bit, Dual Rail-to-Rail DACs with I2C Interface
*/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>

#include "ltc2606.h"
#include "platform_drivers.h"

/**
 * Initialize the device.
 * @param device - The device structure.
 * @param init_param - The structure that contains the device initial
 * 					   parameters.
 * @return 0 in case of success, negative error code otherwise.
 */
int32_t ltc2606_init(struct ltc2606_dev **device,
	struct ltc2606_init_param init_param)
{
	struct ltc2606_dev *dev;
	int32_t ret;

	dev = (struct ltc2606_dev *)malloc(sizeof(*dev));
	if (!dev)
		return -1;

	ret = mbed_i2c_init(&dev->i2c_desc, &init_param.i2c_init);
	dev->resolution_setting = init_param.resolution_setting;

	*device = dev;
	return ret;
}


/**
 * Calculates an LTC2606 DAC code for the desired output voltage.
 * Based on the desired output voltage, the offset, and lsb parameters, return the corresponding DAC code that should be written to the LTC2606.
 * @param dac_voltage		Desired output voltage
 * @param *code				Returned DAC code
 * @return 0 in case of success, negative error code otherwise.
 */
int16_t ltc2606_voltage_to_code(float dac_voltage, uint16_t *code)
{
	
	const float LTC2606_TYPICAL_lsb = (float)(6.2500953695E-5);  										// The LTC2606 typical least significant bit value with 4.096V full-scale
	const float LTC2606_TYPICAL_OFFSET = -0.001;  														// The LTC2606 typical offset voltage
	
	int32_t dac_code, ret;
	float float_code;
	float_code = dac_voltage / LTC2606_TYPICAL_lsb - LTC2606_TYPICAL_OFFSET;    						// 1) Calculate the DAC code
	float_code = (float_code > (floor(float_code) + 0.5)) ? ceil(float_code) : floor(float_code);  		// 2) Round
	dac_code = (int32_t)(float_code);  																	// 3) Convert to unsigned integer
	
	if(dac_code > 65535) {
																										// Requesetd voltage is bigger than reference voltage
		dac_code = 65535;  																				// Return Overflow error
		ret = LTC2606_CODE_OVERFLOW;
	}
	  
	else if(dac_code < 0) {
																										// Requested voltage is lower than offset voltage
		dac_code = 0;  																					// Return Underflow error
		ret = LTC2606_CODE_UNDERFLOW;
	}	
	
	else																								// Requestedd volage is in supported range
		ret = 0;
	
	*code = ((uint16_t)(dac_code));
	return ret;
}


/**
 * Write code to LTC2606
 * @param dev				The device structure.
 * @param dac_command		Command for the DAC
 *							Accepted values:	LTC2606_WRITE_COMMAND
 *												LTC2606_WRITE_UPDATE_COMMAND
 *												
 * @param dac_code			16-bit DAC output voltage code
 * @return 0 in case of success, negative error code otherwise.
 */
int32_t ltc2606_write_code(struct ltc2606_dev *dev, uint8_t dac_command, uint16_t dac_code)
{
	uint8_t data[3];
	
	data[0] = dac_command;  																			// Write command	
	data[1] = dac_code >> 8;  																			// MSB code
	data[2] = dac_code & 0x00FF;  																		// LSB code
	
	return(mbed_i2c_write(dev->i2c_desc, data, 3, 1));
}

/**
 * Power down the device
 * @param device - The device structure.
 * @return 0 in case of success, negative error code otherwise.
 */
int32_t ltc2606_power_down(struct ltc2606_dev *dev)
{
	uint8_t command = LTC2606_POWER_DOWN_COMMAND;
	return (mbed_i2c_write(dev->i2c_desc, &command, 1, 1));
}

/**
 * Power up the device
 * @param device - The device structure.
 * @return 0 in case of success, negative error code otherwise.
 */
int32_t ltc2606_power_up(struct ltc2606_dev *dev)
{
	uint8_t command = LTC2606_UPDATE_COMMAND;
	return (mbed_i2c_write(dev->i2c_desc, &command, 1, 1));
}
