/******************************************************************************
 *Copyright (c)2019 Analog Devices, Inc.  
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:  
 *  - Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.  
 *  - Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of  conditions and the following disclaimer in 
 *    the documentation and/or other materials provided with the 
 *    distribution.    
 *  - Modified versions of the software must be conspicuously marked 
 *    as such.  
 *  - This software is licensed solely and exclusively for use with 
 *    processors/products manufactured by or for Analog Devices, Inc.  
 *  - This software may not be combined or merged with other code 
 *    in any manner that would cause the software to   become subject 
 *    to terms and conditions which differ from those listed here.  
 *  - Neither the name of Analog Devices, Inc. nor the names of its
 *    contributors may be used to endorseor promote products derived 
 *    from this software without specific prior written permission.  
 *  - The use of this software may or may not infringe the patent 
 *    rights of one or more patent holders.  
 *  
 *  This license does not release you from the requirement that you obtain
 *  separate licenses from these patent holders to use this software.
 *  
 * THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES, INC. AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, NON-INFRINGEMENT, TITLE, MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL ANALOG 
 * DEVICES, INC. OR CONTRIBUTORS BE LIABLE FORANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, DAMAGES ARISING OUT OF CLAIMS 
 * OFINTELLECTUAL PROPERTY RIGHTS INFRINGEMENT; PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVERCAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include <mbed.h>
#include <stdint.h>

#include "platform_drivers.h"
#include "adc_drivers.h"
#include "app_config.h"

PinName slave_selects[MAX_SLAVE_SELECTS] = { SPI_CS};
mbed::SPI spi(SPI_MOSI, SPI_MISO, SPI_SCK);
DigitalOut SS(SPI_CS);

/**
 * @brief Select/deselect SS											// Maybe add opportunity to change the mode as well
 * @param select - select the SS
 * @return SUCCESS in case of success, FAILURE otherwise.
 */
int32_t spi_slave_select(uint8_t select)
{
	if (select)
		SS = 1;
	else
		SS = 0;
	
	return SUCCESS;
}

/**
 * @brief Set format											// Maybe add opportunity to change the mode as well
 * @param desc - The SPI descriptor.
 * @param format - SPI frame bit length
 * @return SUCCESS in case of success, FAILURE otherwise.
 */
int32_t set_spi_format(struct spi_desc *desc,
	uint8_t format)
{
	if (format)
		spi.format(16, desc->mode);
		
	else
		spi.format(8, desc->mode);
	
	spi.clear_transfer_buffer();	

	return SUCCESS;		
}


/**
 * @brief Write and read data to/from SPI.
 * @param desc - The SPI descriptor.
 * @param data - The buffer with the transmitted/received data.
 * @param bytes_number - Number of bytes to write/read.
 * @return SUCCESS in case of success, FAILURE otherwise.
 */
int32_t spi_non_mbed(struct spi_desc *desc,
	uint32_t *data,
	uint32_t *rxData)
{
	
	uint32_t a[1], b[1];
	
	a[0] = 0;
	b[0] = 0;
		
	SPI2->DR = 0x00;
	while (!(SPI2->SR & (SPI_SR_TXE))) ;
	while (!(SPI2->SR & (SPI_SR_RXNE))) ;
	while ((SPI2->SR & (SPI_SR_BSY))) ;  
	
	SPI2->DR = 0x01;
	while (!(SPI2->SR & (SPI_SR_TXE))) ;
	while (!(SPI2->SR & (SPI_SR_RXNE))) ;
	while ((SPI2->SR & (SPI_SR_BSY))) ; 
	
	SPI2->DR = 0x02;
	while (!(SPI2->SR & (SPI_SR_TXE))) ;
	while (!(SPI2->SR & (SPI_SR_RXNE))) ;
	while ((SPI2->SR & (SPI_SR_BSY))) ; 
	
	a[0] = 0;
	
	return SUCCESS;
}


#ifdef MBED_ACTIVE

/**
 * @brief Generate miliseconds delay
 * @param msecs - Delay in miliseconds
 */
void _delay_ms(uint32_t msec)
{
	wait_ms(msec);	
}

/**
 * @brief Generate microsecond delay
 * @param msecs - Delay in microseconds
 */
void _delay_us(uint32_t usec)
{
	wait_us(usec);
}

/**
 * @brief Generate second delay
 * @param msecs - Delay in seconds
 */
void _delay_s(float sec)
{
	wait(sec);
}


#endif // MBED_ACTIVE