/******************************************************************************
 *Copyright (c)2019 Analog Devices, Inc.  
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:  
 *  - Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.  
 *  - Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of  conditions and the following disclaimer in 
 *    the documentation and/or other materials provided with the 
 *    distribution.    
 *  - Modified versions of the software must be conspicuously marked 
 *    as such.  
 *  - This software is licensed solely and exclusively for use with 
 *    processors/products manufactured by or for Analog Devices, Inc.  
 *  - This software may not be combined or merged with other code 
 *    in any manner that would cause the software to   become subject 
 *    to terms and conditions which differ from those listed here.  
 *  - Neither the name of Analog Devices, Inc. nor the names of its
 *    contributors may be used to endorseor promote products derived 
 *    from this software without specific prior written permission.  
 *  - The use of this software may or may not infringe the patent 
 *    rights of one or more patent holders.  
 *  
 *  This license does not release you from the requirement that you obtain
 *  separate licenses from these patent holders to use this software.
 *  
 * THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES, INC. AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, NON-INFRINGEMENT, TITLE, MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL ANALOG 
 * DEVICES, INC. OR CONTRIBUTORS BE LIABLE FORANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, DAMAGES ARISING OUT OF CLAIMS 
 * OFINTELLECTUAL PROPERTY RIGHTS INFRINGEMENT; PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVERCAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef ADI_FFT_H_
#define ADI_FFT_H_

#include "stdint.h"
#include "arm_math.h"
#include "stdbool.h"

/**
 ******************************************************************************************************************************************************************
 ******************************************************************************************************************************************************************
 * 
 *			MACROS AND CONSTANT DEFINITIONS
 */

#define N_BITS					24								// Define resolution of the ADC


#define SQRT_2					sqrt(2)
#define ADC_FULL_SCALE			(1 << N_BITS)					// Full scale of the ADC depends on the N_BITS macro, for 24-bit ADC, ADC_FULL_SCALE = 16777216
#define ADC_ZERO_SCALE			(1 << (N_BITS - 1))				// Zero scale of the ADC depends on the N_BITS macro, for 24-bit ADC, ADC_ZERO_SCALE = 8388608

#define DC_BINS					10								// Ignoring certain amount of DC bins for for noise and other calculations
#define FUND_BINS				10								// Power spread of the fundamental, 10 bins from either side of the fundamental
#define HARM_BINS				3								// Power spread of the harmonic, 10 bins from either side of the harmonic


/**
 ******************************************************************************************************************************************************************
 ******************************************************************************************************************************************************************
 * 
 *			TYPES DECLARATIONS
 */

enum fft_windowing_type
{
	BLACKMAN_HARRIS_7TERM,
	RECTANGULAR
};

struct fft_entry									// Structure carying all the necessary data, the FFT work with
{
	float32_t	vref;
	uint16_t	mclk;
	float32_t	bin_width;
	uint32_t	sample_rate;						// Sample rate based on MCLK, MCLK_DIV and Digital filter settings
	uint16_t	fft_length;							// Length of fft = sample_count / 2
	int32_t		codes[4096];						// Codes in range 0 +/- ZERO SCALE
	int32_t		zero_scale_codes[4096];				// Codes shifted up by ZERO SCALE - range ZERO SCALE +/- ZERO SCALE
	float32_t	voltage[4096];						// Voltage before windowing
	float32_t	fft_magnitude[2048];				// Maximum length of FFT magnitude supported by the on-chip DSP = 4096 samples
	float32_t	fft_magnitude_corrected[2048];		// Maginute with windowing correction
	float32_t	fft_dB[2048];						// dB fro plot
	float32_t	fft_input[8192];					// Maximum length of FFT input array supporred by the on-chip DSP = 4096 Real + 4096 Imaginary samples
	float32_t	noise_bins[2048];					// FFT bins excluding DC, fundamental and Harmonics
	enum fft_windowing_type window;					// WIndow type
	bool		fft_done;
};

struct fft_measurements								// Structure carying all the FFT measurements
{
	float32_t	harmonics_power[6]; 				// Harmonics, including their power leakage
	float32_t	harmonics_mag_dbfs[6]; 				// Harmonic magnitudes for THD
	uint16_t	harmonics_freq[6]; 					// Harmonic frequencies for THD
	float32_t	fundamental;  						// Fundamental in volts
	float32_t	pk_spurious_noise; 					// Peak spurious noise (amplitude)
	uint16_t	pk_spurious_freq; 					// Peak Spurious Frequency
	float32_t	THD; 								// Total Harmonic Distortion
	float32_t	SNR; 								// Signal to Noise Ratio
	float32_t	DR; 								// Dynamic Range
	float32_t	SINAD; 								// Signal to Noise And Distortion ratio
	float32_t	SFDR_dbc; 							// Spurious Free Dynamic Range, dBc
	float32_t	SFDR_dbfs; 							// Spurious Free Dynamic Range, dbFS
	float32_t	ENOB; 								// ENOB - Effective Number Of Bits
	float32_t	RMS_noise; 							// same as transition noise
	float32_t	average_bin_noise;
	float32_t	max_amplitude;
	float32_t	min_amplitude;
	float32_t	pk_pk_amplitude;
	float32_t	DC;
	float32_t	transition_noise;
	uint32_t	max_amplitude_LSB;
	uint32_t	min_amplitude_LSB;
	uint32_t	pk_pk_amplitude_LSB;
	int32_t		DC_LSB;
	float32_t	transition_noise_LSB;
};

/**
 ******************************************************************************************************************************************************************
 ******************************************************************************************************************************************************************
 * 
 *			FUNCTION DECLARATIONS
 */

int32_t FFT_init_params(struct fft_entry **fft_entry_init, struct fft_measurements **fft_meas);
void perform_FFT(uint32_t *data, struct fft_entry *fft_data, struct fft_measurements *fft_meas, uint32_t sample_rate);
void FFT_init(uint16_t sample_count, struct fft_entry *fft_data);
void update_FFT_enviroment(uint16_t reference, uint16_t master_clock, struct fft_entry *fft_data);
void static FFT_maginutde_do_dB(struct fft_entry *fft_data, double sum);
void static FFT_calculate_THD(struct fft_entry *fft_data, struct fft_measurements *fft_meas);
void static FFT_calculate_noise(struct fft_entry *fft_data, struct fft_measurements *fft_meas);
float32_t static dbfs_to_volts(float32_t vref, float32_t value);
void static FFT_windowing(struct fft_entry *fft_data, double *sum);
void static FFT_waveform_stat(struct fft_entry *fft_data, struct fft_measurements *fft_meas);
#endif // !ADI_FFT_H_
