/******************************************************************************
 *Copyright (c)2019 Analog Devices, Inc.  
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:  
 *  - Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.  
 *  - Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of  conditions and the following disclaimer in 
 *    the documentation and/or other materials provided with the 
 *    distribution.    
 *  - Modified versions of the software must be conspicuously marked 
 *    as such.  
 *  - This software is licensed solely and exclusively for use with 
 *    processors/products manufactured by or for Analog Devices, Inc.  
 *  - This software may not be combined or merged with other code 
 *    in any manner that would cause the software to   become subject 
 *    to terms and conditions which differ from those listed here.  
 *  - Neither the name of Analog Devices, Inc. nor the names of its
 *    contributors may be used to endorseor promote products derived 
 *    from this software without specific prior written permission.  
 *  - The use of this software may or may not infringe the patent 
 *    rights of one or more patent holders.  
 *  
 *  This license does not release you from the requirement that you obtain
 *  separate licenses from these patent holders to use this software.
 *  
 * THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES, INC. AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, NON-INFRINGEMENT, TITLE, MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL ANALOG 
 * DEVICES, INC. OR CONTRIBUTORS BE LIABLE FORANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, DAMAGES ARISING OUT OF CLAIMS 
 * OFINTELLECTUAL PROPERTY RIGHTS INFRINGEMENT; PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVERCAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef _PCAL9555A_H_
#define _PCAL9555A_H_

#include <stdint.h>
#include "platform_drivers.h"
//#include "adc_drivers.h"

/**
 ******************************************************************************************************************************************************************
 ******************************************************************************************************************************************************************
 * 
 *			MACROS AND CONSTANT DEFINITIONS
 */

//		pcal9555a I2C Address				//	A2		A1		A0
#define PCAL9555A_ADDR 0x40					//	LOW		LOW		LOW
//#define PCAL9555A_ADDR 0x42				//	LOW		LOW		HIGH
//#define PCAL9555A_ADDR 0x44				//	LOW		HIGH	LOW
//#define PCAL9555A_ADDR 0x46				//	LOW		HIGH	HIGH
//#define PCAL9555A_ADDR 0x48				//	HIGH	LOW		LOW
//#define PCAL9555A_ADDR 0x4a				//	HIGH	LOW		HIGH
//#define PCAL9555A_ADDR 0x4c				//	HIGH	HIGH	LOW
//#define PCAL9555A_ADDR 0x4e				//	HIGH	HIGH	HIGH
 
#define INPUT_PORT_0							0x00
#define INPUT_PORT_1							0x01
#define OUTPUT_PORT_0							0x02
#define OUTPUT_PORT_1							0x03
#define POLARITY_INVERSION_0					0x04
#define POLARITY_INVERSION_1					0x05
#define CONFIGURATION_0							0x06
#define CONFIGURATION_1							0x07
#define OUTPUT_DRIVE_STRENGTH_00				0x40
#define OUTPUT_DRIVE_STRENGTH_01				0x41
#define OUTPUT_DRIVE_STRENGTH_10				0x42
#define OUTPUT_DRIVE_STRENGTH_11				0x43
#define INPUT_LATCH_0							0x44
#define INPUT_LATCH_1							0x45
#define PUPD_ENABLE_0							0x46
#define PUPD_ENABLE_1							0x47
#define PUPD_SELECTION_0						0x48
#define PUPD_SELECTION_1						0x49
#define INTERRUPT_MASK_0						0x4A
#define INTERRUPT_MASK_1						0x4B
#define INTERRUPT_STATUS_0						0x4C
#define INTERRUPT_STATUS_1						0x4D
#define OUTPUT_PORT_CONFIG						0x4f

// General GPIO masks for separate GPIOS
#define PCAL9555A_READ_MSK_GPIO(x)				(0x1 << x)						// x as GPIO number
#define PCAL9555A_SHIFT_GPIO(x, a)				((a) >> x)						// x as GPIO number, y as readed value
#define PCAL9555A_WRITE_MSK_GPIO(x)				(0x1 << x)						// x as GPIO number
#define PCAL9555A_WRITE_GPIO(x, y)				(((y) & 0x1) << x)				// x as GPIO number, y as value

// All bits at once
#define PCAL9555A_READ_ALL_MSK					(0xFF << 0)
#define PCAL9555A_WRITE_ALL_MSK					(0xFF << 0)
#define PCAL9555A_WRITE_ALL(x)					(((x) & 0xFF) << 0)				// x as 8-bit value, to be written into all GPIOs

// GPIO output drive strength
#define PCAL9555A_READ_MSK_STRENGTH_GPIO(x)		(0x3 << x)						// x as GPIO number
#define PCAL9555A_SHIFT_STRENGTH_GPIO(x, a)		((a) >> x)						// x as GPIO number, y as readed value
#define PCAL9555A_WRITE_MSK_STRENGTH_GPIO(x)	(0x3 << x)						// x as GPIO number
#define PCAL9555A_WRITE_STRENGTH_GPIO(x, y)		(((y) & 0x3) << x)				// x as GPIO number, y as value

// Pull-up / Pull-down resistor
#define PULL_UP									1
#define PULL_DOWN								0

// I2C address masks read / write
#define PCAL_I2C_READ(x)						(x | 0x01)
#define	PCAL_I2C_WRITE(x)						(x & 0xFE)

#define PCAL_GPIO_OUTPUT						0
#define PCAL_GPIO_INPUT							1

/**
 ******************************************************************************************************************************************************************
 ******************************************************************************************************************************************************************
 * 
 *			TYPES DECLARATIONS
 */

enum pcal9555a_port
{
	PCAL9555A_PORT0		= 0,
	PCAL9555A_PORT1		= 1
}
;

enum pcal9555a_pin
{
	PCAL9555A_PIN0     = 0,
	PCAL9555A_PIN1     = 1,
	PCAL9555A_PIN2     = 2,
	PCAL9555A_PIN3     = 3,
	PCAL9555A_PIN4     = 4,
	PCAL9555A_PIN5     = 5,
	PCAL9555A_PIN6     = 6,
	PCAL9555A_PIN7     = 7,
	PCAL9555A_ALL_PINS = 8
};

enum pcal9555a_drive_strength
{
	OUTPUT_STRENGTH_025 = 0b00,
	OUTPUT_STRENGTH_05  = 0b01,
	OUTPUT_STRENGTH_075 = 0b10,
	OUTPUT_STRENGTH_1   = 0b11
};

struct pcal9555a_dev {
	/* I2C */
	i2c_desc	*i2c_desc;
	uint8_t		resolution_setting;
	
};

struct pcal9555a_init_param {
	/* I2C */
	i2c_init_param	i2c_init;
	/* Device Settings */
	uint8_t		resolution_setting;
};

/**
 ******************************************************************************************************************************************************************
 ******************************************************************************************************************************************************************
 * 
 *			FUNCTION DECLARATIONS
 */

int32_t pcal9555a_init(struct pcal9555a_dev **device, struct pcal9555a_init_param init_param);
int32_t pcal9555a_i2c_reg_read(struct pcal9555a_dev *dev, uint8_t reg_addr, uint8_t *reg_data);
int32_t pcal9555a_i2c_reg_write(struct pcal9555a_dev *dev, uint8_t reg_addr, uint8_t reg_data);
int32_t pcal9555a_i2c_read_mask(struct pcal9555a_dev *dev, uint8_t reg_addr, uint8_t mask, uint8_t *data);
int32_t pcal9555a_i2c_write_mask(struct pcal9555a_dev *dev, uint8_t reg_addr, uint8_t mask, uint8_t data);
int32_t pcal9555a_read_gpio_register(struct pcal9555a_dev *dev, uint8_t port_index, enum pcal9555a_pin gpio, uint8_t *value);
int32_t pcal9555a_write_gpio_register(struct pcal9555a_dev *dev, uint8_t port_index, enum pcal9555a_pin gpio, uint8_t value);
int32_t pcal9555a_set_output_gpio_strength(struct pcal9555a_dev *dev, enum pcal9555a_port port_index, enum pcal9555a_pin gpio, enum pcal9555a_drive_strength strength);

#endif // !_PCAL9555A_H_


