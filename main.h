/******************************************************************************
 *Copyright (c)2019 Analog Devices, Inc.  
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:  
 *  - Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.  
 *  - Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of  conditions and the following disclaimer in 
 *    the documentation and/or other materials provided with the 
 *    distribution.    
 *  - Modified versions of the software must be conspicuously marked 
 *    as such.  
 *  - This software is licensed solely and exclusively for use with 
 *    processors/products manufactured by or for Analog Devices, Inc.  
 *  - This software may not be combined or merged with other code 
 *    in any manner that would cause the software to   become subject 
 *    to terms and conditions which differ from those listed here.  
 *  - Neither the name of Analog Devices, Inc. nor the names of its
 *    contributors may be used to endorseor promote products derived 
 *    from this software without specific prior written permission.  
 *  - The use of this software may or may not infringe the patent 
 *    rights of one or more patent holders.  
 *  
 *  This license does not release you from the requirement that you obtain
 *  separate licenses from these patent holders to use this software.
 *  
 * THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES, INC. AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, NON-INFRINGEMENT, TITLE, MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL ANALOG 
 * DEVICES, INC. OR CONTRIBUTORS BE LIABLE FORANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, DAMAGES ARISING OUT OF CLAIMS 
 * OFINTELLECTUAL PROPERTY RIGHTS INFRINGEMENT; PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVERCAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef _MAIN_H_
#define _MAIN_H_

#include <stdint.h>
#include <string>

using namespace std;

void static go_to_error();
void static print_title();
void static print_prompt();
int32_t static getMenuSelect(uint16_t *menuSelect);
int32_t static getLargeMenuSelect(uint32_t *largeSelect);
void static print_binary(uint8_t number, char *binary_number);
void static menu_1_set_powermode(void);
void static menu_2_set_clock_divider(void);
void static menu_3_set_filter_type(void);
void static set_FIR_filter(void);
void static set_SINC5_filter(void);
void static set_SINC3_filter(void);
void static set_50HZ_rej(void);
void static set_user_defined_FIR(void);
void static menu_4_buffers_controll(void);
void static menu_5_set_default_settings(void);
void static menu_6_vcm_setup(void);
void static menu_7_read_register(void);
void static menu_8_read_data(void);
void static adc_data_read(void);
void static menu_9_reset_ADC(void);
void static menu_10_power_down(void);
void static menu_11_ADC_GPIO(void);
void static GPIO_write(void);
void static GPIO_inout(void);
void static GPIO_settings(void);
void static menu_12_read_master_status(void);
void static menu_13_mclk_vref(void);
void static menu_14_print_measured_data(void);
void static menu_15_set_data_output_mode(void);
void static menu_16_set_diagnostic_mode(void);
void static menu_17_do_the_fft(void);
void static menu_18_fft_settings(void);
void static menu_19_gains_offsets(void);
void static menu_20_check_scratchpad(void);
void static menu_21_piezo_offset(void);
void static menu_22_set_DAC_output(void);
#endif // !_MAIN_H_
