/******************************************************************************
 *Copyright (c)2019 Analog Devices, Inc.  
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:  
 *  - Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.  
 *  - Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of  conditions and the following disclaimer in 
 *    the documentation and/or other materials provided with the 
 *    distribution.    
 *  - Modified versions of the software must be conspicuously marked 
 *    as such.  
 *  - This software is licensed solely and exclusively for use with 
 *    processors/products manufactured by or for Analog Devices, Inc.  
 *  - This software may not be combined or merged with other code 
 *    in any manner that would cause the software to   become subject 
 *    to terms and conditions which differ from those listed here.  
 *  - Neither the name of Analog Devices, Inc. nor the names of its
 *    contributors may be used to endorseor promote products derived 
 *    from this software without specific prior written permission.  
 *  - The use of this software may or may not infringe the patent 
 *    rights of one or more patent holders.  
 *  
 *  This license does not release you from the requirement that you obtain
 *  separate licenses from these patent holders to use this software.
 *  
 * THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES, INC. AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, NON-INFRINGEMENT, TITLE, MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL ANALOG 
 * DEVICES, INC. OR CONTRIBUTORS BE LIABLE FORANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, DAMAGES ARISING OUT OF CLAIMS 
 * OFINTELLECTUAL PROPERTY RIGHTS INFRINGEMENT; PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVERCAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef _ADC_DRIVERS_H_
#define _ADC_DRIVERS_H_

// Our platform driver needs to be C-compatible to work with the drivers
#ifdef __cplusplus
extern "C"
{
#endif

/******************************************************************************/
/*********************************** Includes *********************************/
/******************************************************************************/

#include "stdint.h"

/******************************************************************************/
/********************** Macros and Constants Definitions **********************/
/******************************************************************************/

#define ARD_IO0_PIN			D15
#define ARD_IO1_PIN			D14
#define ARD_IO2_PIN			A3
#define ARD_IO3_PIN			A2
#define ARD_IO4_PIN			A1
#define ARD_IO5_PIN			A0
#define ARD_IO6_PIN			D7
#define ARD_FDA_EN_PIN		D9
#define ARD_FDA_MODE_PIN	D8
#define ARD_IO_INT_PIN		D4
#define ARD_DAC_BUF_EN_PIN	D5
#define ARD_LED1_PIN		D0
#define ARD_LED2_PIN		D1

#define ARD_IO0_ID			2
#define ARD_IO1_ID			3
#define ARD_IO2_ID			4
#define ARD_IO3_ID			5
#define ARD_IO4_ID			6
#define ARD_IO5_ID			7
#define ARD_IO6_ID			8
#define ARD_FDA_ID			9
#define ARD_FDA_MODE_ID		10
#define ARD_IO_INT_ID		11
#define ARD_DAC_BUF_EN_ID	12
#define ARD_LED1_ID			13
#define ARD_LED2_ID			14		
	
#define DRDY_PIN	D2
#define ADC_RST_PIN	D3

#define DRDY_ID		0
#define ADC_RST_ID	1

#define _16bit_SPI	1
#define _8bit_SPI	0	
	
#define ENABLE		1
#define DISABLE		0

/*******************************************************************************/
/**************************** Types Declarations *******************************/
/*******************************************************************************/
	
	
/******************************************************************************/
/************************ Functions Declarations ******************************/
/******************************************************************************/

int32_t spi_read_data(struct spi_desc *desc,
		uint16_t *data,
		uint8_t frames);	

int32_t spi_slave_select(uint8_t select);		

int32_t set_spi_format(struct spi_desc *desc,
		uint8_t format);	
int32_t spi_non_mbed(struct spi_desc *desc,
		uint32_t *data,
		uint32_t *rxData);	
	
void _delay_ms(uint32_t msec);	
void _delay_us(uint32_t usec);	
void _delay_s(float sec);

#ifdef __cplusplus 
}				  
#endif // __cplusplus 	
#endif // !_ADC_DRIVERS_H_