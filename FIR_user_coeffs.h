/******************************************************************************
 *Copyright (c)2019 Analog Devices, Inc.  
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:  
 *  - Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.  
 *  - Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of  conditions and the following disclaimer in 
 *    the documentation and/or other materials provided with the 
 *    distribution.    
 *  - Modified versions of the software must be conspicuously marked 
 *    as such.  
 *  - This software is licensed solely and exclusively for use with 
 *    processors/products manufactured by or for Analog Devices, Inc.  
 *  - This software may not be combined or merged with other code 
 *    in any manner that would cause the software to   become subject 
 *    to terms and conditions which differ from those listed here.  
 *  - Neither the name of Analog Devices, Inc. nor the names of its
 *    contributors may be used to endorseor promote products derived 
 *    from this software without specific prior written permission.  
 *  - The use of this software may or may not infringe the patent 
 *    rights of one or more patent holders.  
 *  
 *  This license does not release you from the requirement that you obtain
 *  separate licenses from these patent holders to use this software.
 *  
 * THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES, INC. AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, NON-INFRINGEMENT, TITLE, MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL ANALOG 
 * DEVICES, INC. OR CONTRIBUTORS BE LIABLE FORANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, DAMAGES ARISING OUT OF CLAIMS 
 * OFINTELLECTUAL PROPERTY RIGHTS INFRINGEMENT; PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVERCAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef _FIR_USER_COEFFS_H_
#define _FIR_USER_COEFFS_H_

#include <stdint.h>

/*
 *  User-defined coefficients for programmable FIR filter, max 56 coeffs
 *  
 *  Please note that, inserted coefficiets will be mirrored afterwards,
 *  so you must insert only one half of all the coefficients.
 *  
 *  Please note your original filer must have ODD count of coefficients,
 *  allowing internal ADC circuitry to mirror the coefficients properly.
 *  
 *	In case of usage lower count of coeffs than 56, please make sure, that
 *	the variable 'count_of_active_coeffs' bellow, carries the correct number
 *	of coeficients, allowing to fill the rest of the coeffs by zeroes
 *
 *	Default coeffs:
 **/
const uint8_t count_of_active_coeffs = 56;

const float programmable_FIR[56] = { 
		
	-9.53674E-07,
	 3.33786E-06,
	 5.48363E-06,
   - 5.48363E-06,
   - 1.54972E-05,
	 5.24521E-06,
	 3.40939E-05,
	 3.57628E-06,
   - 6.17504E-05,
   - 3.05176E-05,
	 9.56059E-05,
	 8.74996E-05,
   - 0.000124693,
   - 0.000186205,
	 0.000128746,
	 0.000333548,
   - 7.70092E-05,
   - 0.000524998,
   - 6.98566E-05,
     0.000738144,
	 0.000353813,
   - 0.000924349,
   - 0.000809193,
	 0.001007795,
	 0.00144887,
   - 0.000886202,
   - 0.002248049,
	 0.000440598,
	 0.00312829,
	 0.000447273,
   - 0.00394845,
   - 0.001870632,
	 0.004499197,
	 0.003867388,
   - 0.004512072,
   - 0.006392241,
	 0.003675938,
	 0.009288311,
   - 0.001663446,
   - 0.012270451,
   - 0.001842737,
	 0.014911652,
	 0.007131577,
   - 0.016633987,
   - 0.014478207,
	 0.016674042,
	 0.024231672,
   - 0.013958216,
   - 0.037100792,
	 0.006659508,
	 0.055086851,
	 0.009580374,
   - 0.085582495,
   - 0.052207232,
	 0.177955151,
	 0.416601658,
};

#endif // !_FIR_USER_COEFFS_H_