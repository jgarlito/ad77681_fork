/******************************************************************************
 *Copyright (c)2019 Analog Devices, Inc.  
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:  
 *  - Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.  
 *  - Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of  conditions and the following disclaimer in 
 *    the documentation and/or other materials provided with the 
 *    distribution.    
 *  - Modified versions of the software must be conspicuously marked 
 *    as such.  
 *  - This software is licensed solely and exclusively for use with 
 *    processors/products manufactured by or for Analog Devices, Inc.  
 *  - This software may not be combined or merged with other code 
 *    in any manner that would cause the software to   become subject 
 *    to terms and conditions which differ from those listed here.  
 *  - Neither the name of Analog Devices, Inc. nor the names of its
 *    contributors may be used to endorseor promote products derived 
 *    from this software without specific prior written permission.  
 *  - The use of this software may or may not infringe the patent 
 *    rights of one or more patent holders.  
 *  
 *  This license does not release you from the requirement that you obtain
 *  separate licenses from these patent holders to use this software.
 *  
 * THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES, INC. AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, NON-INFRINGEMENT, TITLE, MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL ANALOG 
 * DEVICES, INC. OR CONTRIBUTORS BE LIABLE FORANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, DAMAGES ARISING OUT OF CLAIMS 
 * OFINTELLECTUAL PROPERTY RIGHTS INFRINGEMENT; PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVERCAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef _INIT_PARAMS_H_
#define _INIT_PARAMS_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include "ad77681.h"
#include "pcal9555a.h"
#include "ltc2606.h"
#include "piezo_board.h"
	
#include "platform_drivers.h"
#include "adc_drivers.h"
#include <mbed.h>
	
// Init params

// SPI bus init parameters
spi_init_param spi_params = {
	MBED,					// platform
	GENERIC_SPI,			// SPI type
	2,						// SPI ID
	10000000,				// SPI Speed	10 000 000
	SPI_MODE_3,				// SPI Mode 
	SPI_CS,					// SPI CS select
};

// Inital params of a GPIO, to which the DRDY signal of the ADC is connected
gpio_desc gpio_drdy = { 
	GENERIC_GPIO,			// GPIO type
	DRDY_ID,				// GPIO ID
	0,						// GPIO number
	DRDY_PIN,				// GPIO pin 				
};

// Inital params of a GPIO, to which the RST signal of the ADC is connected
gpio_desc gpio_reset = { 
	GENERIC_GPIO,			// GPIO type
	ADC_RST_ID,				// GPIO ID
	0,						// GPIO number
	ADC_RST_PIN,			// GPIO number
};	

// Initial parameters for the ADC AD7768-1
ad77681_init_param init_params = { 
	
	spi_params,					// SPI parameters
	gpio_reset,				// GPIOS
	gpio_drdy,
	AD77681_ECO,				// power_mode
	AD77681_MCLK_DIV_16,		// mclk_div
	AD77681_CONV_CONTINUOUS,	// conv_mode
	AD77681_AIN_SHORT,			// diag_mux_sel
	false,						// conv_diag_sel
	AD77681_CONV_24BIT,			// conv_len
	AD77681_NO_CRC,				// crc_sel
	0,							// status bit
	AD77681_VCM_HALF_VCC,		// VCM setup
	AD77681_AINn_ENABLED,		// AIN- precharge buffer
	AD77681_AINp_ENABLED,		// AIN+ precharge buffer			
	AD77681_BUFn_ENABLED,		// REF- buffer
	AD77681_BUFp_ENABLED,		// REF+ buffer
	AD77681_FIR,				// FIR Filter
	AD77681_SINC5_FIR_DECx32,	// Decimate by 32
	0,							// OS ratio of SINC3
	4096,						// Reference voltage
	16384,						// MCLK in kHz
	32000,						// Sample rate in Hz
	1,							// Data frame bytes
};

// Initial parameters for the IO Extender PCAL9555A's I2C bus
i2c_init_param i2c_params_io = {
		GENERIC_I2C, 									// I2C type
		0, 												// I2C id
		100000, 										// I2C max speed (hz)
		PCAL9555A_ADDR, 								// I2C slave address
};	

// Initial parameters for the IO Extender PCAL9555A itself
pcal9555a_init_param init_params_io_extender = { 
		i2c_params_io, 									// I2C parameters
		0												// Delete this
};	
	
// Initial parameters for the DAC LTC2606's I2C bus	
i2c_init_param i2c_params_dac = {
		GENERIC_I2C, 									// i2c type
		0, 												// i2c id
		100000, 										// i2c max speed (hz)
		LTC2606_WRITE_ADDRESS(LTC2606_I2C_ADDRESS), 	// i2c slave address
};	

// Initial parameters for the DAC LTC2606 itself
ltc2606_init_param init_params_dac = { 
		i2c_params_dac,									// I2C parameters
		0												// Delete this
};	

gpio_desc led1 = { 
		GENERIC_GPIO, 
		ARD_LED1_ID,
		0, 
		ARD_LED1_PIN, 
};

gpio_desc led2 = { 
		GENERIC_GPIO, 
		ARD_LED2_ID,
		0, 
		ARD_LED2_PIN, 
};

arduino_gpios_init gpio_init_param = {
		led1,
		led2,
};		

#ifdef __cplusplus 
}				  
#endif // __cplusplus 	
#endif // !_INIT_PARAMS_H_
