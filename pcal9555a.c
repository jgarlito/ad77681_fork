/******************************************************************************
 *Copyright (c)2019 Analog Devices, Inc.  
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:  
 *  - Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.  
 *  - Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of  conditions and the following disclaimer in 
 *    the documentation and/or other materials provided with the 
 *    distribution.    
 *  - Modified versions of the software must be conspicuously marked 
 *    as such.  
 *  - This software is licensed solely and exclusively for use with 
 *    processors/products manufactured by or for Analog Devices, Inc.  
 *  - This software may not be combined or merged with other code 
 *    in any manner that would cause the software to   become subject 
 *    to terms and conditions which differ from those listed here.  
 *  - Neither the name of Analog Devices, Inc. nor the names of its
 *    contributors may be used to endorseor promote products derived 
 *    from this software without specific prior written permission.  
 *  - The use of this software may or may not infringe the patent 
 *    rights of one or more patent holders.  
 *  
 *  This license does not release you from the requirement that you obtain
 *  separate licenses from these patent holders to use this software.
 *  
 * THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES, INC. AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, NON-INFRINGEMENT, TITLE, MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL ANALOG 
 * DEVICES, INC. OR CONTRIBUTORS BE LIABLE FORANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, DAMAGES ARISING OUT OF CLAIMS 
 * OFINTELLECTUAL PROPERTY RIGHTS INFRINGEMENT; PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVERCAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>

#include "pcal9555a.h"
#include "platform_drivers.h"

/**
 * Initialize the device.
 * @param device - The device structure.
 * @param init_param - The structure that contains the device initial
 * 					   parameters.
 * @return 0 in case of success, negative error code otherwise.
 */
int32_t pcal9555a_init(struct pcal9555a_dev **device, struct pcal9555a_init_param init_param)
{
	struct pcal9555a_dev *dev;
	int32_t status;

	dev = (struct pcal9555a_dev *)malloc(sizeof(*dev));
	if (!dev)
		return -1;

	status = mbed_i2c_init(&dev->i2c_desc, &init_param.i2c_init);
	dev->resolution_setting = init_param.resolution_setting;

	*device = dev;
	return status;
}


/**
 * i2c register read
 * @param dev - The device structure.
 * @param reg_addr - The register address.
 * @param reg_data - The register data.
 * @return 0 in case of success, negative error code otherwise.
 */
int32_t pcal9555a_i2c_reg_read(struct pcal9555a_dev *dev, uint8_t reg_addr, uint8_t *reg_data)
{
	uint8_t buffer;
	int32_t ret;

	buffer = reg_addr;
	
	dev->i2c_desc->slave_address = PCAL_I2C_WRITE(dev->i2c_desc->slave_address);
	ret |= mbed_i2c_write(dev->i2c_desc, &buffer, 1, 1);
	// Descriptor, buffer, byte_length, stop bit
	
	dev->i2c_desc->slave_address = PCAL_I2C_READ(dev->i2c_desc->slave_address);
	ret |= mbed_i2c_read(dev->i2c_desc, &buffer, 1, 0);
	// Descriptor, buffer, byte_length, stop bit
	
	*reg_data = buffer;
	
	return ret;	
}


/**
 * i2c register write
 * @param dev - The device structure.
 * @param reg_addr - The register address.
 * @param reg_data - The register data.
 * @return 0 in case of success, negative error code otherwise.
 */
int32_t pcal9555a_i2c_reg_write(struct pcal9555a_dev *dev, uint8_t reg_addr, uint8_t reg_data)
{
	
	uint8_t buf[2];
	
	dev->i2c_desc->slave_address = PCAL_I2C_WRITE(dev->i2c_desc->slave_address);
	
	buf[0] = reg_addr;
	buf[1] = reg_data;	
	
	// Descriptor, buffer, byte_length, stop bit
	return mbed_i2c_write(dev->i2c_desc, buf, 2, 1);
	
}


/**
 * i2c read from device using a mask.
 * @param dev - The device structure.
 * @param reg_addr - The register address.
 * @param mask - The mask.
 * @param data - The register data.
 * @return 0 in case of success, negative error code otherwise.
 */
int32_t pcal9555a_i2c_read_mask(struct pcal9555a_dev *dev, uint8_t reg_addr, uint8_t mask, uint8_t *data)
{
	
	uint8_t reg_data;
	int32_t ret;

	ret = pcal9555a_i2c_reg_read(dev, reg_addr, &reg_data);
	*data = (reg_data & mask);
	
	return ret;
}


/**
 * i2c write to device using a mask.
 * @param dev - The device structure.
 * @param reg_addr - The register address.
 * @param mask - The mask.
 * @param data - The register data.
 * @return 0 in case of success, negative error code otherwise.
 */
int32_t pcal9555a_i2c_write_mask(struct pcal9555a_dev *dev, uint8_t reg_addr, uint8_t mask, uint8_t data)
{
	
	uint8_t reg_data;
	int32_t ret;
	
	ret |= pcal9555a_i2c_reg_read(dev, reg_addr, &reg_data);
	reg_data &= ~mask;
	reg_data |= data;
	ret |= pcal9555a_i2c_reg_write(dev, reg_addr, reg_data);
	
	return ret;
}


/**															
 * Write to any register: specific bit, or whole register at once
 * Not ment to use separate bit writing for output drive strength registers: 0x40, 0x41, 0x42, 0x43, because of different addressing
 *	- Each single GPIO is not assigned to each single bit
 * @param dev 			The device structure.
 * @param port_index	Register to be written to
 *											
 * @param gpio			Specific GPIO, which will be written
 *						Accepted values:	PCAL9555A_PIN0
 *											PCAL9555A_PIN1
 *											...
 *											PCAL9555A_ALL_PINS
 * @param value			value to be written
 * @return 0 in case of success, negative error code otherwise.
 */
int32_t pcal9555a_write_gpio_register(struct pcal9555a_dev *dev, uint8_t port_index, enum pcal9555a_pin gpio, uint8_t value)
{
	int32_t ret;
	
	if (gpio != PCAL9555A_ALL_PINS)
		ret = pcal9555a_i2c_write_mask(dev, port_index, PCAL9555A_WRITE_MSK_GPIO(gpio), PCAL9555A_WRITE_GPIO(gpio, value));
	else
		ret = pcal9555a_i2c_write_mask(dev, port_index, PCAL9555A_WRITE_ALL_MSK, PCAL9555A_WRITE_ALL(value));
	
	return ret;
}


/**	
 * Read from any register: specific bit, or whole register at once
 * Not ment to use separate bit reading for output drive strength registers: 0x40, 0x41, 0x42, 0x43, because of different addressing
 *	- Each single GPIO is not assigned to each single bit
 * @param dev 			The device structure.
 * @param port_index	Register to be readed from
 *											
 * @param gpio			Specific GPIO, which will be readed
 *						Accepted values:	PCAL9555A_PIN0
 *											PCAL9555A_PIN1
 *											...
 *											PCAL9555A_ALL_PINS
 * @param value			Readed value
 * @return 0 in case of success, negative error code otherwise.
 */
int32_t pcal9555a_read_gpio_register(struct pcal9555a_dev *dev, uint8_t port_index, enum pcal9555a_pin gpio, uint8_t *value)
{
	int32_t ret;	
	//const uint8_t gpio_mask = (gpio == PCAL9555A_ALL_PINS) ? (0xFF << 0) : (0x1 << gpio);
	
	if(gpio != PCAL9555A_ALL_PINS) {
		ret = pcal9555a_i2c_read_mask(dev, port_index, PCAL9555A_READ_MSK_GPIO(gpio), value);
		*value = PCAL9555A_SHIFT_GPIO(gpio, *value);
	}
	else
		ret = pcal9555a_i2c_read_mask(dev, port_index, PCAL9555A_READ_ALL_MSK, value);
	
	return ret;
}

/**															
 * Set the strength of current source for each GPIO, configured as output, separately
 * @param dev 			The device structure.
 * @param port_index	Index of output port: 0 or 1
 *						Accepted values:	PCAL9555A_PORT0
 *											PCAL9555A_PORT0
 *											
 * @param gpio			Specific GPIO, which will be written
 *						Accepted values:	PCAL9555A_PIN0
 *											PCAL9555A_PIN1
 *											...
 *											PCAL9555A_ALL_PINS
 *											
 * @param strength		Strength of the current source at the GPIO's output
 *						Accepted values:	OUTPUT_STRENGTH_025
 *											OUTPUT_STRENGTH_05
 *											OUTPUT_STRENGTH_075
 *											OUTPUT_STRENGTH_1
 *										
 * @return 0 in case of success, negative error code otherwise.
 */

int32_t pcal9555a_set_output_gpio_strength(struct pcal9555a_dev *dev, enum pcal9555a_port port_index, enum pcal9555a_pin gpio, enum pcal9555a_drive_strength strength)
{
	int32_t ret;
	uint8_t value, port = 0;
	
	if (gpio == PCAL9555A_ALL_PINS)
	{
		value = strength | strength << 2 | strength << 4 | strength << 6;  										// Copy desired GPIO output strength settings to all GPIOs
		
		port = OUTPUT_DRIVE_STRENGTH_00 + (port_index * 2);  													// Set the register accroding to the GPIO port, for first half of the GPIOs
		ret |= pcal9555a_i2c_write_mask(dev,																	// Write the same strength settings to the first half of the GPIOs in desired port
										port, 
										PCAL9555A_WRITE_ALL_MSK, 
										PCAL9555A_WRITE_ALL(value));
		port++;  																								// Increase the register for the rest of the GPIOs		
		
		ret |= pcal9555a_i2c_write_mask(dev,																	// Write the same strength settings to the second half of the GPIOs in desired port
										port, 
										PCAL9555A_WRITE_ALL_MSK, 
										PCAL9555A_WRITE_ALL(value));
	}
	
	else
	{
		if (((gpio + 1) * 2) > 8)																				// If GPIO has higher index than 4, incerase port
			{
				port++; 
				gpio -= 4;
			}
		
		port = OUTPUT_DRIVE_STRENGTH_00 + port + (port_index * 2);  											// Set final desired port
		gpio *= 2;  																							// GPIO * 2 because of 2-bits per 1 GPIO
		
		ret = pcal9555a_i2c_write_mask(	dev, 
										port, 
										PCAL9555A_WRITE_MSK_STRENGTH_GPIO(gpio), 
										PCAL9555A_WRITE_STRENGTH_GPIO(gpio, strength));
	}
	
	return ret;
}
